/**
 *
 * SmsPages
 *
 */

import { Paper, Tab, Tabs } from '@material-ui/core';
import LoadingWithText from 'components/LoadingWithText/index';
import NotificationBox from 'components/NotificationBox';
import DashboardLayout from 'layouts/DashboardLayout/Loadable';
import PropTypes from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  getPhonesAction,
  sendMessageAction,
  SendStatusAction,
} from './actions';
import GetSendigMessage from './GetSendingMessage';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';
import makeSelectSmsPages, {
  makeSelectGetSendingMessage,
  makeSelectSmsPagesGetPhone,
} from './selectors';
import SendigMessage from './SendMessage/index';
import { MessagesSpeedStyles, TabStyles } from './styles';
import UpdateSendingSms from './UpdateState';

export function SmsPages({
  getphone,
  getPhonesDataServers,
  dispatch,
  getSending,
}) {
  useInjectReducer({ key: 'smsPages', reducer });
  useInjectSaga({ key: 'smsPages', saga });
  // #region Page
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  // #endregion

  // #region sendMessageStatus State
  const [dataClicked, setDataClicked] = useState(null);
  const [status, setStatus] = useState({
    state: null,
    slug: null,
  });
  const [Clicked, setClicked] = useState(false);
  // #endregion sendMessageStatus State

  // #region Operators &charctesr

  const [operator] = useState({
    To: '',
    MsgBody: '',
    Operator: '',
    types: '',
    lengths: 1,
  });

  const [selectedTabs, setSelectedTabs] = useState(0);

  // #endregion Operators &charctesr

  function handlePageChange(p, s) {
    setPage(p);
    setPageSize(s);
  }
  const isLoading = !!getphone.data;

  const handleChanges = () => {
    if (isLoading === false) {
      getPhonesDataServers();
    }
  };

  // #region statusMessage
  const handleSetStatus = (key, value) => {
    setStatus({ ...status, [key]: value });
  };
  const handleChangeClicek = e => {
    setDataClicked(e.status);
    setClicked(true);
    setStatus({ ...status, slug: e.id });
  };
  const handleSendStatusMessageActivation = () => {
    dispatch(SendStatusAction(status));
  };
  // #endregion statusMessage

  useEffect(() => {
    handleChanges();
  }, [isLoading]);

  return (
    <DashboardLayout>
      <FormattedMessage {...messages.header} />
      <MessagesSpeedStyles>
        <div>
          <NotificationBox />
        </div>
        <TabStyles>
          <Tabs
            component={Paper}
            value={selectedTabs}
            onChange={(e, tabIndex) => {
              setSelectedTabs(tabIndex);
            }}
            indicatorColor="primary"
            textColor="primary"
            className="tabs"
          >
            <Tab label="ارسال ها" />
            <Tab label="ارسال پیام" />
          </Tabs>
          <div>
            {selectedTabs === 1 && (
              <SendigMessage
                operator={operator}
                isLoading={isLoading}
                getphone={getphone}
              />
            )}
          </div>
          <div>
            {selectedTabs === 0 && (
              <div>
                {getSending ? (
                  <GetSendigMessage
                    data={getSending.data}
                    total={getSending.total}
                    handleClikedProduct={handleChangeClicek}
                    onChangePage={handlePageChange}
                    page={page}
                    size={pageSize}
                  />
                ) : (
                  <span>
                    <LoadingWithText />
                  </span>
                )}
              </div>
            )}
          </div>
        </TabStyles>
        {Clicked && (
          <UpdateSendingSms
            dataClick={dataClicked}
            onClose={() => setClicked(false)}
            defaultValue={status.state || ''}
            onSetDataSending={handleSendStatusMessageActivation}
            handleChange={defaultValue =>
              handleSetStatus('state', defaultValue)
            }
          />
        )}
      </MessagesSpeedStyles>
    </DashboardLayout>
  );
}

SmsPages.propTypes = {
  dispatch: PropTypes.func.isRequired,
  getPhonesDataServers: PropTypes.func.isRequired,
  getphone: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  getSending: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    .isRequired,
};

const mapStateToProps = createStructuredSelector({
  smsPages: makeSelectSmsPages(),
  getphone: makeSelectSmsPagesGetPhone(),
  getSending: makeSelectGetSendingMessage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPhonesDataServers: loading => dispatch(getPhonesAction(loading)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SmsPages);
