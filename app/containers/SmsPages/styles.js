import { Dialog } from '@material-ui/core';
import styled from 'styled-components';
import '../../layouts/dana.css';

export const MessagesSpeedStyles = styled.div`
  .text-messages {
    width: 100%;
    border-radius: 17px;
    margin: 10px auto;
    align-items: center;
    min-width: 300px;
    display: grid;
    max-width: 100%;
    padding: 10px;
    direction: ${props => props.dir};
  }
  .wrapers-msg {
    max-width: 800px;
    margin: 1rem;
  }
  .wraper-select-phone {
    padding: 5%;
    margin: auto;
    align-items: center;
    align-content: center;
    display: grid;
    gap: 5%;
    justify-content: space-between;
    color: #ee5465;
  }
  .btn-message {
    margin: auto;
    display: grid;
    width: 200px;
    background-color: aquamarine;
  }

  .btn-message-speed {
  }
`;

export const WraperSelect = styled.div`
  height: 30px;
`;
export const TabStyles = styled.div`
  height: 30px;
`;

export const SelectedPhonesStyles = styled.div`
  width: 300px;
  padding: 10px;
  border: 1px solid #ee54f3;
  border-radius: 10px;
  height: 60px;
  .input-select-phone {
    margin: auto;
    width: 100%;
  }
  .select-phone-opens {
    width: 100%;
    margin: auto;
    text-align: center;
    label {
      width: 100%;
    }
  }
  .form-class {
    width: 100%;
  }
`;

export const UploadBannersFileStyles = styled.div`
  .images-input {
    display: none;
    background-color: rgb(49, 164, 198);
  }
  .btn-uploads {
    margin: auto;
    left: 10%;
    position: relative;
    background-color: rgb(49, 164, 198);
  }
  & .MuiCardActions-root {
    position: relative;
    left: 5%;
    margin-top: 4%;
    display: grid;
    justify-content: left;
  }
  .btn-up-file {
    background-color: rgb(49, 164, 198);
  }
`;

export const ViewStylesMessaging = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, auto));
  text-align: center;
  margin: auto;
  font-family: 'fateme';
  color: cadetblue;
  .chip-sending-messages {
  }

  .chip-wrpe {
    font-size: 14px;
    font-family: 'mobina';
    min-width: 79px;
  }
`;

export const GetSendigMessageStyles = styled.div`
  .wraper-Table-message {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(60px, auto));
    text-align: center;
    margin: 5px;
  }
  .td-wraper {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(104px, auto));
    text-align: center;
    margin: 5px;
  }
  .msg-class {
    width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    max-height: 100px;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    position: relative;
  }
`;

export const UpdateStateStyles = styled(Dialog)`
  width: auto;
  min-width: 350px;
  .wraper {
    min-width: 300px;
    height: 200px;
  }
`;
