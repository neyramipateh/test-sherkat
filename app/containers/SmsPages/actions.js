/*
 *
 * SmsPages actions
 *
 */

import {
  GET_PHONES,
  GET_PHONES_SUCCESS,
  GET_PHONES_FAILE,
  SEND_MESSAGE_ACTION,
  SEND_MESSAGE_ACTION_SUCCESS,
  SEND_MESSAGE_ACTION_FAILE,
  GET_SENDMESSAGING,
  GET_SENDMESSAGING_SUCCESS,
  GET_SENDMESSAGING_FAILE,
  SEND_STATUS_MESSAGING,
  SEND_STATUS_MESSAGING_SUCCESS,
  SEND_STATUS_MESSAGING_FAILE,
} from './constants';

export function getPhonesAction() {
  return {
    type: GET_PHONES,
  };
}
export function getPhonesSuccessAction(data) {
  return {
    type: GET_PHONES_SUCCESS,
    data,
  };
}
export function getPhonesFaileAction(error) {
  return {
    type: GET_PHONES_FAILE,
    error,
  };
}

// #region  SendMessage Payamk
export function sendMessageAction(params) {
  return {
    type: SEND_MESSAGE_ACTION,
    params,
  };
}
export function sendMessageSuccessAction(data) {
  return {
    type: SEND_MESSAGE_ACTION_SUCCESS,
    data,
  };
}
export function sendMessageFaileAction(error) {
  return {
    type: SEND_MESSAGE_ACTION_FAILE,
    error,
  };
}
// #endregion  SendMessage Payamk

// #region  GetSendingMessage

export function getSendingAction(params) {
  return {
    type: GET_SENDMESSAGING,
    params,
  };
}
export function getSendingSuccessAction(data) {
  return {
    type: GET_SENDMESSAGING_SUCCESS,
    data,
  };
}
export function getSendingFaileAction(error) {
  return {
    type: GET_SENDMESSAGING_FAILE,
    error,
  };
}

// #endregion GetSendingMessage

// #region  SendStatuseMessage
export function SendStatusAction(slug) {
  return {
    type: SEND_STATUS_MESSAGING,
    slug,
  };
}
export function SendStatusSuccessAction(data) {
  return {
    type: SEND_STATUS_MESSAGING_SUCCESS,
    data,
  };
}
export function SendStatusFaileAction(error) {
  return {
    type: SEND_STATUS_MESSAGING_FAILE,
    error,
  };
}

// #endregion SendStatuseMessage
