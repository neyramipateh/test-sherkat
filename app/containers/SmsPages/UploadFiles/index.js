/**
 *
 * UploadFiles
 *
 */

import { Button, CardActions } from '@material-ui/core';
import { getDownloadURL, ref, uploadBytesResumable } from 'firebase/storage';
import { countBy } from 'lodash';
import PropTypes from 'prop-types';
import React, { memo, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { storage } from '../../../components/FireBase/index';
import { UploadBannersFileStyles } from '../styles';

function UploadFiles({ value, onChange }) {
  const [filesData, setFilesData] = useState(value);
  const [progres, setProgres] = useState(0);
  const [count, setCount] = useState(0);
  const fileInput = useRef();

  const handleChangeBanner = () => {
    const reader = new FileReader();
    const file = fileInput.current.files[0];

    reader.onload = async events => {
      const text = events.currentTarget.result;
      const countSmS = text.split('\n').length;
      setCount(countSmS);
      setFilesData(text);
    };

    reader.readAsText(file);
    // TODO::add file manajers from to backend  fireBase
    // const randomstring = Math.random()

    //   .toString(36)
    //   .slice(-file.name.length);

    // uploadFiles(file, randomstring);
  };
  useEffect(() => {
    onChange(filesData);
  }, [filesData]);
  return (
    <UploadBannersFileStyles>
      <div>
        <h5>تعداد شماره ها</h5>
        <h5>{count}</h5>
      </div>
      <CardActions>
        <input
          defaultValue={filesData || ''}
          accept="text/*"
          className="images-input"
          id="contained-button-file"
          type="file"
          name="banner"
          // disabled={loading}
          ref={fileInput}
          onChange={events => handleChangeBanner(events)}
        />
        <label htmlFor="contained-button-file">
          <Button variant="contained" className="btn-up-file" component="span">
            Upload{progres}
          </Button>
        </label>
      </CardActions>
    </UploadBannersFileStyles>
  );
}

UploadFiles.propTypes = {
  value: PropTypes.any,
  onChange: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  // updateproducts: makeSelectUpdateProduct(),
  // uploadbanner: makeSelectBannerUpload(),
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
export default compose(
  withConnect,
  memo,
)(UploadFiles);
