import { Button, CardActions } from '@material-ui/core';
import { app, storage } from 'components/FireBase';
import { getDownloadURL, ref, uploadBytesResumable } from 'firebase/storage';
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { UploadImageStyles } from '../styles';

function ImageUp({ value }) {
  const [filesData, setFilesData] = useState(value);
  const [progres, setProgres] = useState(0);
  const fileInput = useRef();

  const handleChangeBanner = () => {
    const file = fileInput.current.files[0];
    const randomstring = Math.random();
    const storageRef = ref(storage, `images/${randomstring}/${file.name}`);
    const uploadTask = uploadBytesResumable(storageRef, file);
    uploadTask.on(
      'state_management',
      snapshot => {
        const progress = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
        );
        setProgres(progress);
      },
      error => {
        console.log(error);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then(url => console.log(url));
      },
    );

    // const storageRefs = ref(storage, `/${randomstring}/${file.name}`);
    // console.log(storageRef, reader);
  };

  return (
    <UploadImageStyles>
      <h1>uoloadBanner</h1>
      <CardActions>
        <input
          defaultValue={filesData || ''}
          //   accept="video/*"
          className="images-input"
          id="contained-button-file"
          type="file"
          name="banner"
          // disabled={loading}
          ref={fileInput}
          onChange={events => handleChangeBanner(events)}
        />
        <label htmlFor="contained-button-file">
          <Button variant="contained" className="btn-up-file" component="span">
            Upload{progres}
          </Button>
        </label>
      </CardActions>
      <h4>{progres}</h4>
    </UploadImageStyles>
  );
}

ImageUp.propTypes = {
  value: PropTypes.any,
  dispatch: PropTypes.func,
};

export default ImageUp;
