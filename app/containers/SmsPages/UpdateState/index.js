import React, { useState, memo } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@material-ui/core';
import { UpdateStateStyles } from '../styles';

const types = [
  { id: '1', title: 'pending', lable: 'در حال بررسی' },
  { id: '2', title: 'accepte', lable: 'تایید شده' },
  { id: '3', title: 'error', lable: 'با مشکل رو به رو شده' },
  { id: '4', title: 'block', lable: 'قابلیت ارسال وجود ندارد' },
];
function UpdateSendingSms({
  dataClick,
  onClose,
  onSetDataSending,
  handleChange,
}) {
  // console.log(dataClick);
  const [status, setStatus] = useState({
    state: null,
  });
  const isUpdated = dataClick !== null ? dataClick : status;

  const handleSetChange = val => {
    if (val) {
      setStatus(val);
      handleChange(val);
    }
  };
  return (
    <UpdateStateStyles open>
      <div className="wraper">
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Activation</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={isUpdated.state || ''}
            label="Status"
            onChange={e => handleSetChange(e.target.value)}
          >
            {types &&
              Object.values(types).map(items => (
                <MenuItem key={items.id} value={items.title}>
                  {items.lable}
                </MenuItem>
              ))}
          </Select>
        </FormControl>
      </div>
      <Button onClick={onClose}>بستن</Button>
      <Button onClick={onSetDataSending}>ذخیره</Button>
    </UpdateStateStyles>
  );
}

UpdateSendingSms.propTypes = {
  dataClick: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  onClose: PropTypes.func.isRequired,
  onSetDataSending: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default memo(UpdateSendingSms);
