/*
 *
 * SmsPages constants
 *
 */

export const DEFAULT_ACTION = 'app/SmsPages/DEFAULT_ACTION';

// #region  GetPhones
export const GET_PHONES = 'app/SmsPages/GET_PHONES';
export const GET_PHONES_SUCCESS = 'app/SmsPages/GET_PHONES_SUCCESS';
export const GET_PHONES_FAILE = 'app/SmsPages/GET_PHONES_FAILE';
// #endregion  GetPhones

// #region  SEND_MESSAGE
export const SEND_MESSAGE_ACTION = 'app/SmsPages/SEND_MESSAGE_ACTION';
export const SEND_MESSAGE_ACTION_SUCCESS =
  'app/SmsPages/SEND_MESSAGE_ACTION_SUCCESS';
export const SEND_MESSAGE_ACTION_FAILE =
  'app/SmsPages/SEND_MESSAGE_ACTION_FAILE';
// #endregion  SEND_MESSAGE

// #region Get SendingMessage
export const GET_SENDMESSAGING = 'app/SmsPages/GET_SENDMESSAGING';
export const GET_SENDMESSAGING_SUCCESS =
  'app/SmsPages/GET_SENDMESSAGING_SUCCESS';
export const GET_SENDMESSAGING_FAILE = 'app/SmsPages/GET_SENDMESSAGING_FAILE';
// #endregion Get SendingMessage

// #region SendStatuseMessage
export const SEND_STATUS_MESSAGING = 'app/SmsPages/SEND_STATUS_MESSAGING';
export const SEND_STATUS_MESSAGING_SUCCESS =
  'app/SmsPages/SEND_STATUS_MESSAGING_SUCCESS';
export const SEND_STATUS_MESSAGING_FAILE =
  'app/SmsPages/SEND_STATUS_MESSAGING_FAILE';
// #endregion SendStatuseMessage
