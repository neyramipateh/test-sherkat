import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the smsPages state domain
 */

const selectSmsPagesDomain = state => state.smsPages || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by SmsPages
 */

const makeSelectSmsPages = () =>
  createSelector(
    selectSmsPagesDomain,
    substate => substate,
  );
export const makeSelectSmsPagesGetPhone = () =>
  createSelector(
    selectSmsPagesDomain,
    substate => substate.getphones.data,
  );

export const makeSelectGetSendingMessage = () =>
  createSelector(
    selectSmsPagesDomain,
    substate => substate.getsendingmessage.data,
  );

export default makeSelectSmsPages;
export { selectSmsPagesDomain };
