/*
 *
 * SmsPages reducer
 *
 */
import produce from 'immer';
import {
  GET_PHONES,
  GET_PHONES_SUCCESS,
  GET_PHONES_FAILE,
  SEND_MESSAGE_ACTION,
  SEND_MESSAGE_ACTION_SUCCESS,
  SEND_MESSAGE_ACTION_FAILE,
  GET_SENDMESSAGING,
  GET_SENDMESSAGING_SUCCESS,
  GET_SENDMESSAGING_FAILE,
  SEND_STATUS_MESSAGING,
  SEND_STATUS_MESSAGING_SUCCESS,
  SEND_STATUS_MESSAGING_FAILE,
} from './constants';

export const initialState = {
  getphones: {
    data: [],
    error: null,
  },
  sendmessage: {
    params: null,
    data: [],
    error: null,
  },
  getsendingmessage: {
    params: null,
    data: [],
    error: null,
  },
  sendstatusmessage: {
    slug: null,
    data: null,
    error: null,
  },
};

/* eslint-disable default-case, no-param-reassign */
const smsPagesReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_PHONES:
        draft.getphones.error = null;
        break;
      case GET_PHONES_SUCCESS:
        draft.getphones.data = action.data;
        draft.getphones.error = null;
        break;
      case GET_PHONES_FAILE:
        draft.getphones.error = action.error;
        break;
      case SEND_MESSAGE_ACTION:
        draft.sendmessage.params = action.params;
        draft.sendmessage.error = null;
        break;
      case SEND_MESSAGE_ACTION_SUCCESS:
        draft.sendmessage.data = action.data;
        draft.sendmessage.error = null;
        break;
      case SEND_MESSAGE_ACTION_FAILE:
        draft.sendmessage.error = action.error;
        break;
      // #region  GetSendingMessage
      case GET_SENDMESSAGING:
        draft.getsendingmessage.params = action.params;
        draft.getsendingmessage.error = null;
        break;
      case GET_SENDMESSAGING_SUCCESS:
        draft.getsendingmessage.data = action.data;
        draft.getsendingmessage.params = null;
        break;
      case GET_SENDMESSAGING_FAILE:
        draft.getsendingmessage.error = action.error;
        draft.getsendingmessage.params = null;
        break;
      // #endregion  GetSendingMessage
      // #region  SendStatuseMessage
      case SEND_STATUS_MESSAGING:
        draft.sendstatusmessage.slug = action.slug;
        draft.sendstatusmessage.error = null;
        break;
      case SEND_STATUS_MESSAGING_SUCCESS:
        draft.sendstatusmessage.data = action.data;
        draft.sendstatusmessage.slug = null;
        break;
      case SEND_STATUS_MESSAGING_FAILE:
        draft.sendstatusmessage.error = action.error;
        draft.sendstatusmessage.slug = null;
        break;
      // #endregion  SendStatuseMessage
    }
  });

export default smsPagesReducer;
