import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { getSendingAction } from '../actions';
import { GetSendigMessageStyles } from '../styles';

const columns = [
  { name: 'id', title: 'واحد' },
  { name: 'mciCount', title: 'همراه اول' },
  { name: 'irancellCount', title: ' ایرانسل' },
  {
    name: 'message',
    title: 'توضیحات',
    className: 'msg-class',
    cast: v => (v.indexOf('.') ? v.slice(0, v.indexOf('.')) : v.slice(0, 50)),
  },
  { name: 'types', title: 'زبان' },
  { name: 'operator', title: 'اپراتور' },
  { name: 'countSmS', title: 'تعداد پیام' },
  {
    name: 'status',
    title: 'وضیعت',
    cast: v => (v ? v.state : null),
  },
];
function GetSendigMessage({
  data,
  dispatch,
  total,
  handleClikedProduct,
  onChangePage,
  page,
  size,
}) {
  function handleChangePage(e, newPage) {
    onChangePage(newPage + 1, size);
  }

  function handleChangeRowsPerPage(e) {
    onChangePage(1, parseInt(e.target.value, 10));
  }

  useEffect(() => {
    dispatch(getSendingAction({ page, size }));
  }, [page]);
  return (
    <GetSendigMessageStyles>
      <Grid>
        <Table>
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  className="padd-inputs"
                  key={column.name}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.title}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data &&
              data.map(product => (
                <TableRow
                  hover
                  role="checkbox"
                  tabIndex={-1}
                  key={product.id}
                  onClick={() => handleClikedProduct(product)}
                >
                  {columns.map(column => {
                    const value = product[column.name];
                    return (
                      <TableCell
                        className={column.className}
                        key={column.name}
                        align={column.align}
                        dir={column.dir}
                      >
                        {column.cast ? column.cast(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </Grid>
      {data && (
        <TablePagination
          rowsPerPageOptions={[10, 25, 50]}
          component="div"
          count={total || 0}
          rowsPerPage={size}
          page={page - 1}
          labelRowsPerPage=""
          labelDisplayedRows={() => `صفحه ${page}`}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      )}
    </GetSendigMessageStyles>
  );
}

GetSendigMessage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onChangePage: PropTypes.func,
  handleClikedProduct: PropTypes.func,
  page: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  total: PropTypes.number,
};

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(GetSendigMessage);
