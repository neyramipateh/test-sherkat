import { Button, TextareaAutosize } from '@material-ui/core';
import LoadingWithText from 'components/LoadingWithText';
import NotificationBox from 'components/NotificationBox';
import PropTypes from 'prop-types';
import React, { memo, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { sendMessageAction } from '../actions';
import SelectePhones from '../SelectePhones';
import { ViewStylesMessaging } from '../styles';
import UploadFiles from '../UploadFiles';
import LenghtCharecters from './length';
import { SendingMessageStyles } from './styles';

function SendigMessage({ operator, isLoading, getphone, dispatch }) {
  const textRef = useRef(1);
  const [languages, setLanguages] = useState({
    lengths: 0,
    types: null,
    fitures: null,
    MsgBody: null,
    To: null,
    Operator: null,
    charecter: 0,
  });

  const datalengh = languages.types === 'persian' ? 70 : 160;

  function handleOnOk() {
    setLanguages({ ...languages, MsgBody: textRef.current.value });
    console.log(languages);
  }

  const handleToSendMsg = () => {
    if (languages) {
      dispatch(sendMessageAction(languages));
    }
  };

  function justPersian(vals) {
    const persiaAlpha = new RegExp('[\u0600-\u06FF]');
    const eng = new RegExp('[a-zA-Z\\s]*$');
    const language = {
      persian,
      english,
    };
    const persian = vals.match(persiaAlpha);
    const english = vals.match(eng);
    if (vals in language) {
      language[vals]();
    }

    const tafahom = vals.includes(persian) === true ? 'persian' : 'english';
    const direction = tafahom === 'persian' ? 'rtl' : 'ltr';

    setLanguages({
      ...languages,
      lengths: Math.floor(textRef.current.value.length / datalengh) + 1,
      types: tafahom,
      langs: direction,
      charecter: textRef.current.value.length,
    });
  }

  const handleSetUpdate = (key, value) => {
    setLanguages({ ...languages, [key]: value });
  };

  return (
    <SendingMessageStyles>
      <div>
        <NotificationBox />
      </div>
      <div className="wraper-select-phone">
        {isLoading === true ? (
          <SelectePhones
            require
            name="Operator"
            data={getphone}
            value={operator.Operator || ''}
            onChange={value => handleSetUpdate('Operator', value)}
          />
        ) : (
          <LoadingWithText />
        )}
      </div>
      <div className="wrapers-msg">
        <Button className="btn btn-ok" onClick={handleOnOk}>
          تایید
        </Button>
        <span>متن خود را تایید کنید</span>
        <TextareaAutosize
          type="string"
          defaultValue={operator.MsgBody || ''}
          name="MsgBody"
          onChange={e => justPersian(e.currentTarget.value)}
          aria-label="minimum height"
          minRows={4}
          maxRows={5}
          placeholder="متن خود را وارد نمایید "
          className="text-messages"
          dir={languages.langs}
          ref={textRef}
        />
      </div>
      <ViewStylesMessaging>
        <h5>{languages.types}</h5>
        <div className="chip-sending-messages">
          <LenghtCharecters
            lengths={textRef.current.value || ''}
            datalengh={datalengh}
            languages={languages}
          />
        </div>
      </ViewStylesMessaging>
      <div>
        <UploadFiles
          name="To"
          defaultValue={operator.To || ''}
          onChange={defaultValue => handleSetUpdate('To', defaultValue)}
        />
      </div>
      <div className="btn-message">
        <Button
          variant="outlined"
          type="button"
          className="btn-message-speed"
          onClick={handleToSendMsg}
        >
          Send
        </Button>
      </div>
    </SendingMessageStyles>
  );
}
SendigMessage.propTypes = {
  operator: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
  getphone: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  dispatch: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SendigMessage);
