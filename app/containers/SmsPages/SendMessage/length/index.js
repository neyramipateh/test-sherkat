/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { LengthCharterStyles } from '../styles';

function LenghtCharecters({ datalengh, languages, lengths }) {
  const _len = lengths.length;
  const packets = Math.ceil(Math.floor(_len / datalengh) + 1);
  const mchars = _len - packets * datalengh;

  return (
    <LengthCharterStyles>
      <h5>{Math.floor(packets)}</h5>
      <h5>{Math.floor(mchars)}</h5>
      <h5>{Math.floor(languages.charecter)}</h5>
    </LengthCharterStyles>
  );
}

LenghtCharecters.propTypes = {
  datalengh: PropTypes.any,
  lengths: PropTypes.any,
  languages: PropTypes.any,
};

export default LenghtCharecters;
