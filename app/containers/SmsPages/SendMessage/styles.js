import styled from 'styled-components';
export const LengthCharterStyles = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, auto));
  margin: auto;
`;
export const SendingMessageStyles = styled.div`
  .btn {
    color: aquamarine;
    background-color: #31a4c6;
  }
  .btn-ok {
    font-family: yara;
  }
`;
