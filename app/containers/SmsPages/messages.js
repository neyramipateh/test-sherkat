/*
 * SmsPages Messages
 *
 * This contains all the text for the SmsPages container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.SmsPages';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'مدیریت ارسال پیام ',
  },
});
