import { getPhonesApi } from 'api/phones';
import {
  getSendingMessageApi,
  sendMessageApi,
  getSendingStatusMessageApi,
} from 'api/Sms';
import {
  NOTIFICATION_TYPE_ERROR,
  NOTIFICATION_TYPE_SUCCESS,
} from 'components/NotificationBox';
import { notificationShowAction } from 'containers/App/actions';

import { put, call, takeLatest } from 'redux-saga/effects';
import {
  getPhonesFaileAction,
  getPhonesSuccessAction,
  getSendingFaileAction,
  getSendingSuccessAction,
  SendStatusFaileAction,
  SendStatusSuccessAction,
} from './actions';
import {
  GET_PHONES,
  GET_SENDMESSAGING,
  SEND_MESSAGE_ACTION,
  SEND_STATUS_MESSAGING,
} from './constants';

// #region  GetPhones
function* getPhonesActions() {
  try {
    const responce = yield call(getPhonesApi);
    yield put(getPhonesSuccessAction(responce.data));
  } catch (error) {
    yield put(getPhonesFaileAction(error.responce));
  }
}

// #endregion  GetPhones

// #region SendingMessage
function* sendMessageActions({ params }) {
  try {
    const responce = yield call(sendMessageApi, params);
    yield put(getPhonesSuccessAction(responce.data));
  } catch (error) {
    yield put(getPhonesFaileAction(error.responce));
  }
}
// #endregion  SendingMessage

// #region  GetSendingMessage

function* getSendingMessageActions({ params }) {
  try {
    const responce = yield call(getSendingMessageApi, params);
    yield put(getSendingSuccessAction(responce.data));
  } catch (error) {
    yield put(getSendingFaileAction(error.responce));
  }
}

// #endregion  GetSendingMessage

// #region  SendStatuseMessage

function* sendStatuseMessageActions({ slug }) {
  try {
    const responce = yield call(getSendingStatusMessageApi, slug);
    yield put(SendStatusSuccessAction(responce.data));
    yield put(
      notificationShowAction(
        `        پس از تایید پیام شما در صف ارسال قرار خواهد گرفت,${
          responce.message
        }`,
        NOTIFICATION_TYPE_SUCCESS,
      ),
    );
  } catch (error) {
    yield put(
      notificationShowAction(`${error.message}`, NOTIFICATION_TYPE_ERROR),
    );
    yield put(SendStatusFaileAction(error.responce));
  }
}

// #endregion  SendStatuseMessage
export default function* smsPagesSaga() {
  yield takeLatest(GET_PHONES, getPhonesActions);
  yield takeLatest(SEND_MESSAGE_ACTION, sendMessageActions);
  yield takeLatest(GET_SENDMESSAGING, getSendingMessageActions);
  yield takeLatest(SEND_STATUS_MESSAGING, sendStatuseMessageActions);
  // See example in containers/HomePage/saga.js
}
