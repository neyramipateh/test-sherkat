import {
  FormControl,
  Input,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core';
import LoadingWithText from 'components/LoadingWithText';
import NotificationBox from 'components/NotificationBox';
import PropTypes from 'prop-types';
import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { SelectedPhonesStyles } from '../styles';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function SelectPhone({ onChange, value, data }) {
  const [to, setTo] = useState(value);

  const handleSetChange = val => {
    console.log(val);

    if (val) {
      setTo(val);
      onChange(val);
    }
  };

  return (
    <SelectedPhonesStyles>
      <div>
        <NotificationBox />
      </div>
      <FormControl className="form-class">
        <InputLabel id="demo-mutiple-chip-label" className="input-select-phone">
          شماره تماس
        </InputLabel>
        <Select
          labelId="demo-mutiple-chip-label"
          id="demo-mutiple-chip"
          className="select-phone-opens"
          value={to}
          input={<Input id="select-multiple-chip" />}
          MenuProps={MenuProps}
          onChange={e => handleSetChange(e.target.value)}
        >
          {data.data && data ? (
            Object.values(data.data).map(name => (
              <MenuItem
                key={name.id}
                value={`${name.subnumber.phone}${name.numbers}`}
              >
                {name.subnumber.phone}
                {name.numbers}
              </MenuItem>
            ))
          ) : (
            <div className="loading-data">
              <LoadingWithText />
            </div>
          )}
        </Select>
      </FormControl>
    </SelectedPhonesStyles>
  );
}

SelectPhone.propTypes = {
  data: PropTypes.object.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}
const withStore = connect(
  mapStateToProps,
  mapDispatchToProps,
);
export default compose(
  withStore,
  memo,
)(SelectPhone);
