import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { WraperSelect } from '../styles';

function SelectedIn({ items, onchange, value }) {
  const [data, setData] = useState(items);
  useEffect(() => {
    onchange();
  }, []);
  return (
    <WraperSelect>
      <h1>{data.id}</h1>
      <h1>{value}</h1>
    </WraperSelect>
  );
}

SelectedIn.propTypes = {
  onchange: PropTypes.any,
  value: PropTypes.any,
  items: PropTypes.any,
};

export default SelectedIn;
