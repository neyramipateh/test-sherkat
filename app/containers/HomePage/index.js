/* eslint-disable func-names */
/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import Pusher from 'pusher-js';
import Echo from 'laravel-echo';
import { getAuth } from 'utils/auth';
import ImageUp from '../SmsPages/UploadFiles/ImageUp/index';
function HomePage() {
  const auth = getAuth();

  useEffect(() => {
    Pusher.logToConsole = true;
    const pusher = new Pusher('de9995c320edcf82031a', {
      cluster: 'ap1',
    });
    const channel = pusher.subscribe(`status.${auth.me.id}`);
    channel.bind('status.event', function(data) {
      console.log(JSON.stringify(data));
    });
    // window.Echo = new Echo({
    //   broadcaster: 'pusher',
    //   key: 'de9995c320edcf82031a',
    //   wsHost: window.location.hostname,
    //   wsPort: 6001,
    //   forceTLS: false,
    //   disableStats: true,
    // });
    // window.Echo.private(`status.${auth.me.id}`).listen('status.event', e => {
    //   console.log(e);
    // });
  }, []);

  // const messages = () => {};
  return (
    <div>
      <Helmet>
        <title>صفحه اصلی</title>
        <meta name="description" content="خانه" />
      </Helmet>
      <h1>salam man rezam</h1>
      <div>
        <ImageUp />
      </div>
    </div>
  );
}

HomePage.propTypes = {};

export default HomePage;
