/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import RenderError from 'components/RenderError';
import SubLine from 'containers/Multipale/SubLine/Loadable';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import Loading from '../../components/Loading';
import GlobalStyle from '../../global-styles';
import reducer from './reducer';
import * as routes from './routes';
import saga from './saga';

const HomePage = lazy(() => import('containers/HomePage/Loadable'));
const SMSPages = lazy(() => import('../SmsPages/Loadable'));
const LoginPage = lazy(() => import('containers/LoginPage/Loadable'));
const DashboardPage = lazy(() => import('containers/DashboardPage/Loadable'));
const Multipale = lazy(() => import('containers/Multipale/Loadable'));
const PanelPages = lazy(() => import('containers/PanelPages/Loadable'));

function App() {
  useInjectReducer({ key: 'app', reducer });
  useInjectSaga({ key: 'app', saga });

  return (
    <div>
      <RenderError />
      <Switch>
        <Suspense fallback={<Loading />}>
          <Route
            exact
            path={routes.DASHBOARD_ROUTE}
            component={DashboardPage}
          />
          <Route exact path={routes.HOME_ROUTE} component={HomePage} />
          <Route exact path={routes.LOGIN_ROUTE} component={LoginPage} />
          <Route exact path={routes.MULTIPLE} component={Multipale} />
          <Route exact path={routes.ROUTE_MANAGE_SMS} component={SMSPages} />
          <Route exact path={routes.ROUTE_SUBLINE} component={SubLine} />
          <Route
            exact
            path={routes.ROUTE_MANAGE_PANEL}
            component={PanelPages}
          />
        </Suspense>
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </div>
  );
}

export default App;
