// import { take, call, put, select } from 'redux-saga/effects';

import { addTagApi, getTagsApi } from 'api/tags';
import {
  deleteUserApi,
  getUsersApi,
  logoutApi,
  resetUserPasswordApi,
  unregisterUserApi,
  updateUserApi,
  getUserMeApi,
} from 'api/users';
import {
  NOTIFICATION_TYPE_ERROR,
  NOTIFICATION_TYPE_SUCCESS,
} from 'components/NotificationBox';
import { push } from 'connected-react-router';
import { makeSelectTags, makeSelectCategories } from 'containers/App/selectors';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { setAuth } from 'utils/auth';
import { addCategoriesApi, getCategoriesApi } from '../../api/category';

import {
  addCategoryFailAction,
  addCategorySuccessAction,
  addTagFailAction,
  addTagSuccessAction,
  deleteUserFailAction,
  deleteUserSuccessAction,
  getCategoriesFailAction,
  getCategoriesSuccessAction,
  getTagsFailAction,
  getTagsSuccessAction,
  getUserMeFailAction,
  getUserMeSuccessAction,
  getUsersFailAction,
  getUsersSuccessAction,
  logoutFailAction,
  logoutSuccessAction,
  notificationShowAction,
  resetUserPasswordFailAction,
  resetUserPasswordSuccessAction,
  unregisterUserFailAction,
  updateUserFailAction,
  updateUserSuccessAction,
} from './actions';
import {
  ADD_CATEGORY,
  ADD_TAG,
  DELETE_USER,
  GET_CATEGORIES,
  GET_TAGS,
  GET_USERS,
  GET_USER_ME,
  LOGOUT_ACTION,
  RESET_USER_PASSWORD,
  UNREGISTER_USER,
  UPDATE_USER,
} from './constants';
import { LOGIN_ROUTE } from './routes';

// Individual exports for testing

export function* getUsers({ params }) {
  try {
    const response = yield call(getUsersApi, params);
    yield put(getUsersSuccessAction(response.data));
  } catch (error) {
    yield put(getUsersFailAction(error));
  }
}

export function* updateUsers({ params }) {
  try {
    const response = yield call(updateUserApi, params);
    yield put(updateUserSuccessAction(response.data));

    yield put(
      notificationShowAction(
        'به روز رسانی اطلاعات کاربری با موفقیت انجام شد',
        NOTIFICATION_TYPE_SUCCESS,
      ),
    );
  } catch (error) {
    yield put(
      notificationShowAction(
        'به روز رسانی اطلاعات کاربری با خطا مواجه شد',
        NOTIFICATION_TYPE_ERROR,
      ),
    );
    yield put(updateUserFailAction(error));
  }
}

export function* resetUserPassword({ params }) {
  try {
    const response = yield call(resetUserPasswordApi, params);
    yield put(resetUserPasswordSuccessAction(response.data));

    yield put(
      notificationShowAction(
        'بازنشانی گذر واژه با موفقیت انجام شد',
        NOTIFICATION_TYPE_SUCCESS,
      ),
    );
  } catch (error) {
    yield put(
      notificationShowAction(
        'بازنشانی گذر واژه با مشکل مواجه شد',
        NOTIFICATION_TYPE_ERROR,
      ),
    );
    yield put(resetUserPasswordFailAction(error));
  }
}

export function* unregisterUser() {
  try {
    yield call(unregisterUserApi);
    setAuth();
    yield put(push(LOGIN_ROUTE));
    yield put(logoutSuccessAction());
  } catch (error) {
    yield put(unregisterUserFailAction(error));
  }
}

export function* deleteUser({ params }) {
  try {
    yield call(deleteUserApi, params);
    yield put(deleteUserSuccessAction(true));
    yield put(
      notificationShowAction(
        'حذف کاربر با موفقیت انجام شد',
        NOTIFICATION_TYPE_SUCCESS,
      ),
    );
  } catch (error) {
    yield put(deleteUserFailAction(error));
    yield put(
      notificationShowAction(
        'حذف کاربر با خطا مواجه شد',
        NOTIFICATION_TYPE_ERROR,
      ),
    );
  }
}

export function* getUserMe() {
  try {
    const response = yield call(getUserMeApi);
    yield put(getUserMeSuccessAction(response.data));
  } catch (error) {
    yield put(getUserMeFailAction(error));
  }
}

export function* logout() {
  try {
    yield call(logoutApi);
    setAuth();
    yield put(push(LOGIN_ROUTE));
    yield put(logoutSuccessAction());
  } catch (error) {
    yield put(logoutFailAction(error));
  }
}

function* getTags() {
  try {
    const response = yield call(getTagsApi);
    yield put(getTagsSuccessAction(response.data));
  } catch (error) {
    yield put(getTagsFailAction(error));
  }
}

function* addTag({ tag }) {
  if (tag) {
    try {
      const response = yield call(addTagApi, tag);
      yield put(addTagSuccessAction(response.data));
      const { data } = yield select(makeSelectTags());
      data.push({ ...response.data, isNew: true });
      yield put(getTagsSuccessAction(data));
    } catch (error) {
      yield put(addTagFailAction(error));
    }
  }
}

function* addCategories({ category }) {
  if (category) {
    try {
      const response = yield call(addCategoriesApi, category);
      yield put(addCategorySuccessAction(response.data));
      const { data } = yield select(makeSelectCategories());
      data.push({ ...response.data, isNew: true });
      yield put(getCategoriesSuccessAction(data));
    } catch (error) {
      yield put(addCategoryFailAction(error));
    }
  }
}

function* getCategories() {
  try {
    const response = yield call(getCategoriesApi);
    yield put(getCategoriesSuccessAction(response.data));
  } catch (error) {
    yield put(getCategoriesFailAction(error));
  }
}

export default function* app() {
  yield takeLatest(GET_TAGS, getTags);
  yield takeLatest(ADD_TAG, addTag);

  yield takeLatest(GET_CATEGORIES, getCategories);
  yield takeLatest(ADD_CATEGORY, addCategories);
  // yield takeLatest(EDIT_CATEGORY, editCategory);

  yield takeLatest(GET_USERS, getUsers);
  yield takeLatest(UPDATE_USER, updateUsers);
  yield takeLatest(RESET_USER_PASSWORD, resetUserPassword);
  yield takeLatest(DELETE_USER, deleteUser);
  yield takeLatest(UNREGISTER_USER, unregisterUser);
  yield takeLatest(LOGOUT_ACTION, logout);
  yield takeLatest(GET_USER_ME, getUserMe);
}
