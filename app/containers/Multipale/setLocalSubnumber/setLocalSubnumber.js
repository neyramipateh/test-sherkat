export function setSub(SubData) {
  if (SubData) {
    localStorage.setItem('Sub', JSON.stringify(SubData));
  } else {
    localStorage.removeItem('Sub');
  }
}

export function getSub() {
  try {
    return JSON.parse(localStorage.getItem('Sub'));
  } catch (error) {
    // nothing
  }

  return null;
}
export function setPrice(PriceData) {
  if (PriceData) {
    localStorage.setItem('Price', JSON.stringify(PriceData));
  } else {
    localStorage.removeItem('Price');
  }
}

export function getPrice() {
  try {
    return JSON.parse(localStorage.getItem('Price'));
  } catch (error) {
    // nothing
  }

  return null;
}

export default {
  getPrice,
  setPrice,
};
