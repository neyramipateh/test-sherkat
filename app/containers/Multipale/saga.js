import {
  addSubNameSubLinersApi,
  getCategortApi,
  getRangeApi,
  getSublinersPageApi,
  getSubNameApi,
  sendSubCategoryApi,
  sendSubLineApi,
} from 'api/subName';
import { getPriceApi, getSubnumberApi } from 'api/subnumber';
import { setTarifApi } from 'api/tariffe';
import {
  NOTIFICATION_TYPE_ERROR,
  NOTIFICATION_TYPE_SUCCESS,
} from 'components/NotificationBox';
import { notificationShowAction } from 'containers/App/actions';
import { call, put, takeLatest } from 'redux-saga/effects';
import {
  addSubNameSubLineFailAction,
  addSubNameSubLineSuccessAction,
  getCategoryNumberFailAction,
  getCategoryNumberSuccessAction,
  getPriceFailAction,
  getPriceSuccessAction,
  getRangeNumberFailAction,
  getRangeNumberSuccessAction,
  getSubLinersFailAction,
  getSubLinersSuccessAction,
  getSubNameFailAction,
  getSubNameSuccessAction,
  getSubNumberFailAction,
  getSubNumberSuccessAction,
  sendSubLineFailAction,
  sendSubLineSuccessAction,
  sendSyncSubCategoryFailAction,
  sendSyncSubCategorySuccessAction,
  setTariffFaileAction,
  setTariffSuccessAction,
} from './actions';
import {
  GET_CATEGORYNUMBER_ACTION,
  GET_PRICE_ACTION,
  GET_RANGENUMBER_ACTION,
  GET_SUBLINERS_ACTION,
  GET_SUBNAME_ACTION,
  GET_SUBNUMBER_TARRIFS,
  SEND_ADD_SUBNAME_ACTION,
  SEND_SUBCATEGORYSYNC_ACTION,
  SEND_SUBLINE_ACTION,
  SET_TARRIFS,
} from './constants';
import { setPrice, setSub } from './setLocalSubnumber/setLocalSubnumber';

// Individual exports for

function* setTarrifSaga({ params }) {
  try {
    const response = yield call(setTarifApi, params);
    yield put(setTariffSuccessAction(response.data));

    yield put(
      setTariffSuccessAction(
        'به روز رسانی محصولات با موفقیت انجام شد',
        NOTIFICATION_TYPE_SUCCESS,
      ),
    );
  } catch (error) {
    yield put(
      notificationShowAction(
        'به روز رسانی اطلاعات کاربری با خطا مواجه شد',
        NOTIFICATION_TYPE_ERROR,
      ),
    );
    yield put(setTariffFaileAction(error.response));
  }
}
function* getSubNumber() {
  try {
    const responce = yield call(getSubnumberApi);
    yield put(getSubNumberSuccessAction(responce.data));
    setSub(responce.data);
  } catch (error) {
    yield put(getSubNumberFailAction(error.responce));
  }
}
function* getPrice() {
  try {
    const responce = yield call(getPriceApi);
    yield put(getPriceSuccessAction(responce.data));
    setPrice(responce.data);
  } catch (error) {
    yield put(getPriceFailAction(error.responce));
  }
}
function* getSubName() {
  try {
    const responce = yield call(getSubNameApi);
    yield put(getSubNameSuccessAction(responce.data));
  } catch (error) {
    yield put(getSubNameFailAction(error.responce));
  }
}
function* geCategoryNumber() {
  try {
    const responce = yield call(getCategortApi);
    yield put(getCategoryNumberSuccessAction(responce.data));
  } catch (error) {
    yield put(getCategoryNumberFailAction(error.responce));
  }
}
function* geRangeNumber() {
  try {
    const responce = yield call(getRangeApi);
    yield put(getRangeNumberSuccessAction(responce.data));
  } catch (error) {
    yield put(getRangeNumberFailAction(error.responce));
  }
}
function* sendSubLineSaga({ params }) {
  try {
    const responce = yield call(sendSubLineApi, params);
    yield put(sendSubLineSuccessAction(responce.data));
  } catch (error) {
    yield put(sendSubLineFailAction(error.responce));
  }
}

function* sendSyncSubCategorySaga({ params }) {
  try {
    const responce = yield call(sendSubCategoryApi, params);
    yield put(sendSyncSubCategorySuccessAction(responce.data));
  } catch (error) {
    yield put(sendSyncSubCategoryFailAction(error.responce));
  }
}

function* addSubNumberSublineSaga({ params }) {
  try {
    const responce = yield call(addSubNameSubLinersApi, params);
    yield put(addSubNameSubLineSuccessAction(responce.data));

    yield put(
      notificationShowAction(
        'به روز رسانی اطلاعات با موفقیت انجام شد',
        NOTIFICATION_TYPE_SUCCESS,
      ),
    );
  } catch (error) {
    yield put(
      notificationShowAction(
        'به روز رسانی اطلاعات کاربری با خطا مواجه شد',
        NOTIFICATION_TYPE_ERROR,
      ),
    );
    yield put(addSubNameSubLineFailAction(error.responce));
  }
}

function* getSubLinersSaga({ params }) {
  try {
    const responce = yield call(getSublinersPageApi, params);
    yield put(getSubLinersSuccessAction(responce.data));

    yield put(
      notificationShowAction(
        'به روز رسانی اطلاعات با موفقیت انجام شد',
        NOTIFICATION_TYPE_SUCCESS,
      ),
    );
  } catch (error) {
    yield put(
      notificationShowAction(
        'به روز رسانی اطلاعات کاربری با خطا مواجه شد',
        NOTIFICATION_TYPE_ERROR,
      ),
    );
    yield put(getSubLinersFailAction(error.responce));
  }
}

export default function* multipaleSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(SET_TARRIFS, setTarrifSaga);
  yield takeLatest(GET_SUBNUMBER_TARRIFS, getSubNumber);
  yield takeLatest(GET_PRICE_ACTION, getPrice);
  yield takeLatest(GET_SUBNAME_ACTION, getSubName);
  yield takeLatest(GET_CATEGORYNUMBER_ACTION, geCategoryNumber);
  yield takeLatest(GET_RANGENUMBER_ACTION, geRangeNumber);
  yield takeLatest(SEND_SUBLINE_ACTION, sendSubLineSaga);
  yield takeLatest(SEND_SUBCATEGORYSYNC_ACTION, sendSyncSubCategorySaga);
  yield takeLatest(SEND_ADD_SUBNAME_ACTION, addSubNumberSublineSaga);
  yield takeLatest(GET_SUBLINERS_ACTION, getSubLinersSaga);
}
