/*
 *
 * Multipale reducer
 *
 */
import produce from 'immer';
import {
  SET_TARRIFS,
  SET_TARRIFS_SUCCESS,
  SET_TARRIFS_FAILE,
  GET_TARRIFS,
  GET_TARRIFS_SUCCESS,
  GET_TARRIFS_FAILE,
  GET_SUBNUMBER_TARRIFS,
  GET_SUBNUMBER_TARRIFS_SUCCESS,
  GET_SUBNUMBER_TARRIFS_FAILE,
  GET_PRICE_ACTION,
  GET_PRICE_ACTION_SUCCESS,
  GET_PRICE_ACTION_FAILE,
  GET_SUBNAME_ACTION_SUCCESS,
  GET_SUBNAME_ACTION_FAILE,
  GET_SUBNAME_ACTION,
  GET_CATEGORYNUMBER_ACTION,
  GET_CATEGORYNUMBER_ACTION_SUCCESS,
  GET_CATEGORYNUMBER_ACTION_FAILE,
  GET_RANGENUMBER_ACTION,
  GET_RANGENUMBER_ACTION_SUCCESS,
  GET_RANGENUMBER_ACTION_FAILE,
  SEND_SUBLINE_ACTION,
  SEND_SUBLINE_ACTION_SUCCESS,
  SEND_SUBLINE_ACTION_FAILE,
  SEND_SUBCATEGORYSYNC_ACTION,
  SEND_SUBCATEGORYSYNC_ACTION_SUCCESS,
  SEND_SUBCATEGORYSYNC_ACTION_FAILE,
  SEND_ADD_SUBNAME_ACTION,
  SEND_ADD_SUBNAME_ACTION_SUCCESS,
  SEND_ADD_SUBNAME_ACTION_FAILE,
  GET_SUBLINERS_ACTION,
  GET_SUBLINERS_ACTION_SUCCESS,
  GET_SUBLINERS_ACTION_FAILE,
} from './constants';

export const initialState = {
  tariff: {
    params: null,
    data: [],
    error: null,
  },
  tariffsget: {
    data: [],
    error: null,
  },
  getsubnumber: {
    data: [],
    error: null,
  },
  getprice: {
    data: null,
    error: null,
  },
  getsubname: {
    data: [],
    error: null,
  },
  getcategorynumber: {
    data: [],
    error: null,
  },
  getrange: {
    data: [],
    error: null,
  },
  sendsublin: {
    params: null,
    data: [],
    error: null,
  },
  syncsubcategory: {
    params: null,
    data: [],
    error: null,
  },
  addsubname: {
    params: null,
    data: [],
    error: null,
  },
  getsubliner: {
    params: null,
    data: [],
    error: null,
  },
};

/* eslint-disable default-case, no-param-reassign */
const multipaleReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SET_TARRIFS:
        draft.tariff.params = action.params;
        draft.tariff.error = null;
        draft.tariff.data = null;
        break;
      case SET_TARRIFS_SUCCESS:
        draft.tariff.params = null;
        draft.tariff.data = action.data;
        draft.tariff.error = null;
        break;
      case SET_TARRIFS_FAILE:
        draft.tariff.params = null;
        draft.tariff.data = null;
        draft.tariff.error = action.error;
        break;
      case GET_TARRIFS:
        draft.tariffsget.error = null;
        break;
      case GET_TARRIFS_SUCCESS:
        draft.tariffsget.data = action.data;
        draft.tariffsget.error = null;
        break;
      case GET_TARRIFS_FAILE:
        draft.tariffsget.error = action.error;
        break;
      case GET_SUBNUMBER_TARRIFS:
        draft.getsubnumber.error = null;
        break;
      case GET_SUBNUMBER_TARRIFS_SUCCESS:
        draft.getsubnumber.data = action.data;

        break;
      case GET_SUBNUMBER_TARRIFS_FAILE:
        draft.getsubnumber.error = action.error;
        break;
      case GET_PRICE_ACTION:
        draft.getprice.error = null;
        break;
      case GET_PRICE_ACTION_SUCCESS:
        draft.getprice.data = action.data;

        break;
      case GET_PRICE_ACTION_FAILE:
        draft.getprice.error = action.error;
        break;
      // #region SubNumber
      case GET_SUBNAME_ACTION:
        draft.getsubname.error = null;
        break;
      case GET_SUBNAME_ACTION_SUCCESS:
        draft.getsubname.data = action.data;

        break;
      case GET_SUBNAME_ACTION_FAILE:
        draft.getsubname.error = action.error;
        break;
      // #endregion SubNumber
      // #region CategoryNumber
      case GET_CATEGORYNUMBER_ACTION:
        draft.getcategorynumber.error = null;
        break;
      case GET_CATEGORYNUMBER_ACTION_SUCCESS:
        draft.getcategorynumber.data = action.data;

        break;
      case GET_CATEGORYNUMBER_ACTION_FAILE:
        draft.getcategorynumber.error = action.error;
        break;
      // #endregion CategoryNumber
      // #region RANGENumber
      case GET_RANGENUMBER_ACTION:
        draft.getrange.error = null;
        break;
      case GET_RANGENUMBER_ACTION_SUCCESS:
        draft.getrange.data = action.data;

        break;
      case GET_RANGENUMBER_ACTION_FAILE:
        draft.getrange.error = action.error;
        break;
      // #endregion RANGENumber
      // #region send Subline
      case SEND_SUBLINE_ACTION:
        draft.sendsublin.params = action.params;
        draft.sendsublin.error = null;
        draft.sendsublin.data = null;
        break;
      case SEND_SUBLINE_ACTION_SUCCESS:
        draft.sendsublin.params = null;
        draft.sendsublin.data = action.data;
        draft.sendsublin.error = null;
        break;
      case SEND_SUBLINE_ACTION_FAILE:
        draft.sendsublin.params = null;
        draft.sendsublin.data = null;
        draft.sendsublin.error = action.error;
        break;
      // #endregion send Subline
      // #region send syncsubcategory
      case SEND_SUBCATEGORYSYNC_ACTION:
        draft.syncsubcategory.params = action.params;
        draft.syncsubcategory.error = null;
        draft.syncsubcategory.data = null;
        break;
      case SEND_SUBCATEGORYSYNC_ACTION_SUCCESS:
        draft.syncsubcategory.params = null;
        draft.syncsubcategory.data = action.data;
        draft.syncsubcategory.error = null;
        break;
      case SEND_SUBCATEGORYSYNC_ACTION_FAILE:
        draft.syncsubcategory.params = null;
        draft.syncsubcategory.data = null;
        draft.syncsubcategory.error = action.error;
        break;
      // #endregion send syncsubcategory
      // #region AddSubName FromSubLine
      case SEND_ADD_SUBNAME_ACTION:
        draft.addsubname.params = action.params;
        draft.addsubname.error = null;
        draft.addsubname.data = null;
        break;
      case SEND_ADD_SUBNAME_ACTION_SUCCESS:
        draft.addsubname.params = null;
        draft.addsubname.data = action.data;
        draft.addsubname.error = null;
        break;
      case SEND_ADD_SUBNAME_ACTION_FAILE:
        draft.addsubname.params = null;
        draft.addsubname.data = null;
        draft.addsubname.error = action.error;
        break;
      // #endregion AddSubName FromSubLine
      // #region getSubLinersActions
      case GET_SUBLINERS_ACTION:
        draft.getsubliner.params = action.params;
        draft.getsubliner.error = null;
        break;
      case GET_SUBLINERS_ACTION_SUCCESS:
        draft.getsubliner.params = null;
        draft.getsubliner.data = action.data;
        draft.getsubliner.error = null;
        break;
      case GET_SUBLINERS_ACTION_FAILE:
        draft.getsubliner.params = null;
        draft.getsubliner.data = null;
        draft.getsubliner.error = action.error;
        break;
      // #endregion getSubLinersActions
    }
  });

export default multipaleReducer;
