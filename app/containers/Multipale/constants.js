/*
 *
 * Multipale constants
 *
 */
// const ActionTypes = {

// };

// export default ActionTypes;
export const SET_TARRIFS = 'app/Multipale/SET_TARRIFS';
export const SET_TARRIFS_SUCCESS = 'app/Multipale/SET_TARRIFS_SUCCESS';
export const SET_TARRIFS_FAILE = 'app/Multipale/SET_TARRIFS_FAILE';

export const GET_TARRIFS = 'app/Multipale/GET_TARRIFS';
export const GET_TARRIFS_SUCCESS = 'app/Multipale/GET_TARRIFS_SUCCESS';
export const GET_TARRIFS_FAILE = 'app/Multipale/GET_TARRIFS_FAILE';

export const GET_SUBNUMBER_TARRIFS = 'app/Multipale/GET_SUBNUMBER_TARRIFS';
export const GET_SUBNUMBER_TARRIFS_SUCCESS =
  'app/Multipale/GET_SUBNUMBER_TARRIFS_SUCCESS';
export const GET_SUBNUMBER_TARRIFS_FAILE =
  'app/Multipale/GET_SUBNUMBER_TARRIFS_FAILE';

export const GET_PRICE_ACTION = 'app/Multipale/GET_PRICE_ACTION';
export const GET_PRICE_ACTION_SUCCESS =
  'app/Multipale/GET_PRICE_ACTION_SUCCESS';
export const GET_PRICE_ACTION_FAILE = 'app/Multipale/GET_PRICE_ACTION_FAILE';

// #region get SubName
export const GET_SUBNAME_ACTION = 'app/Multipale/GET_SUBNAME_ACTION';
export const GET_SUBNAME_ACTION_SUCCESS =
  'app/Multipale/GET_SUBNAME_ACTION_SUCCESS';
export const GET_SUBNAME_ACTION_FAILE =
  'app/Multipale/GET_SUBNAME_ACTION_FAILE';
// #endregion get SubName
// #region get CATEGORYNUMBER
export const GET_CATEGORYNUMBER_ACTION =
  'app/Multipale/GET_CATEGORYNUMBER_ACTION';
export const GET_CATEGORYNUMBER_ACTION_SUCCESS =
  'app/Multipale/GET_CATEGORYNUMBER_ACTION_SUCCESS';
export const GET_CATEGORYNUMBER_ACTION_FAILE =
  'app/Multipale/GET_CATEGORYNUMBER_ACTION_FAILE';
// #endregion get CATEGORYNUMBER
// #region get RANGENUMBER
export const GET_RANGENUMBER_ACTION = 'app/Multipale/GET_RANGENUMBER_ACTION';
export const GET_RANGENUMBER_ACTION_SUCCESS =
  'app/Multipale/GET_RANGENUMBER_ACTION_SUCCESS';
export const GET_RANGENUMBER_ACTION_FAILE =
  'app/Multipale/GET_RANGENUMBER_ACTION_FAILE';
// #endregion get RANGENUMBER
// #region get  SEND_SUBLINE
export const SEND_SUBLINE_ACTION = 'app/Multipale/SEND_SUBLINE_ACTION';
export const SEND_SUBLINE_ACTION_SUCCESS =
  'app/Multipale/SEND_SUBLINE_ACTION_SUCCESS';
export const SEND_SUBLINE_ACTION_FAILE =
  'app/Multipale/SEND_SUBLINE_ACTION_FAILE';
// #endregion get  SEND_SUBLINE
// #region get  SEND_PRICE_ID_SYNC_SUBCATEGORY
export const SEND_SUBCATEGORYSYNC_ACTION =
  'app/Multipale/SEND_SUBCATEGORYSYNC_ACTION';
export const SEND_SUBCATEGORYSYNC_ACTION_SUCCESS =
  'app/Multipale/SEND_SUBCATEGORYSYNC_ACTION_SUCCESS';
export const SEND_SUBCATEGORYSYNC_ACTION_FAILE =
  'app/Multipale/SEND_SUBCATEGORYSYNC_ACTION_FAILE';
// #endregion get  SEND_PRICE_ID_SYNC_SUBCATEGORY
// #region get  Send AddSubName FromSubLine
export const SEND_ADD_SUBNAME_ACTION = 'app/Multipale/SEND_ADD_SUBNAME_ACTION';
export const SEND_ADD_SUBNAME_ACTION_SUCCESS =
  'app/Multipale/SEND_ADD_SUBNAME_ACTION_SUCCESS';
export const SEND_ADD_SUBNAME_ACTION_FAILE =
  'app/Multipale/SEND_ADD_SUBNAME_ACTION_FAILE';
// #endregion get  Send AddSubName FromSubLine
// #region getSubLinersActions
export const GET_SUBLINERS_ACTION = 'app/Multipale/GET_SUBLINERS_ACTION';
export const GET_SUBLINERS_ACTION_SUCCESS =
  'app/Multipale/GET_SUBLINERS_ACTION_SUCCESS';
export const GET_SUBLINERS_ACTION_FAILE =
  'app/Multipale/GET_SUBLINERS_ACTION_FAILE';
// #endregion getSubLinersActions
