import { Button, InputAdornment, TextField } from '@material-ui/core';
import { EditAttributes } from '@material-ui/icons';
import { PropTypes } from 'prop-types';
import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { getSubNumberAction, setTariffAction } from '../actions';
import {
  makeGetSubNumberMultipale,
  makeSelectTariffsMultipale,
} from '../selectors';
import SubNumbers from '../SubNumbers';

function Tariffes({ dispatch, subnumbers }) {
  const [tariffs, setTariffs] = useState({
    name: null,
    price: null,
    subnumber: '',
  });

  const handleSetTariffsActions = (key, value) => {
    setTariffs({ ...tariffs, [key]: value });
  };
  const handelpush = () => {
    dispatch(setTariffAction(tariffs));
  };

  return (
    <div>
      <Button className="title-tariff" onClick={handelpush}>
        ثبت تعرفه جدید
      </Button>
      <div className="wraper-tarffe">
        <div className="tarrif-name">
          <TextField
            required
            variant="outlined"
            label="نام تعرفه"
            name="name"
            id="name"
            value={tariffs.name || ''}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EditAttributes />
                </InputAdornment>
              ),
            }}
            onChange={e => handleSetTariffsActions('name', e.target.value)}
          />
        </div>
        <div className="tarrif-price">
          <TextField
            required
            variant="outlined"
            label="قیمت پایه تعرفه"
            name="price"
            value={tariffs.price || ''}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EditAttributes />
                </InputAdornment>
              ),
            }}
            onChange={e => handleSetTariffsActions('price', e.target.value)}
          />
        </div>
      </div>
      <div>
        {subnumbers && (
          <SubNumbers
            dataItem={subnumbers.data}
            value={tariffs.subnumber}
            handleChange={value => handleSetTariffsActions('subnumber', value)}
          />
        )}
      </div>
      <div className="btn-tarrif">
        <Button onClick={handelpush} className="btn-success">
          Senden
        </Button>
      </div>
    </div>
  );
}

Tariffes.propTypes = {
  dispatch: PropTypes.func,
  subnumbers: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

const mapStateToProps = createStructuredSelector({
  multipale: makeSelectTariffsMultipale(),
  subnumbers: makeGetSubNumberMultipale(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getSubNUmberFromServer: () => dispatch(getSubNumberAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Tariffes);
