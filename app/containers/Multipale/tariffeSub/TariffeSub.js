/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import PropTypes from 'prop-types';
import React from 'react';
import { StylesWraperTariffes } from '../styles';

function TariffeSub({ items, handleClikedProduct }) {
  return (
    <StylesWraperTariffes>
      <div className="wraper-tariff-ul">
        <ul className="ul-tsub" onClick={() => handleClikedProduct(items)}>
          <li className="li-tsub">{items.id}</li>
          <li className="li-tsub">{items.name}</li>
          <li className="li-tsub">{items.price}</li>
          <li className="li-tsub">{items.subnumber.phone}</li>
          <li className="li-tsub">{items.subnumber.title}</li>
        </ul>
      </div>
    </StylesWraperTariffes>
  );
}

TariffeSub.propTypes = {
  items: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  handleClikedProduct: PropTypes.func,
};

export default TariffeSub;
