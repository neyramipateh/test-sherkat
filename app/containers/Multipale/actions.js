/*
 *
 * Multipale actions
 *
 */

import {
  SET_TARRIFS,
  SET_TARRIFS_SUCCESS,
  SET_TARRIFS_FAILE,
  GET_TARRIFS,
  GET_TARRIFS_SUCCESS,
  GET_TARRIFS_FAILE,
  GET_SUBNUMBER_TARRIFS,
  GET_SUBNUMBER_TARRIFS_SUCCESS,
  GET_SUBNUMBER_TARRIFS_FAILE,
  GET_PRICE_ACTION,
  GET_PRICE_ACTION_SUCCESS,
  GET_PRICE_ACTION_FAILE,
  GET_SUBNAME_ACTION,
  GET_SUBNAME_ACTION_SUCCESS,
  GET_SUBNAME_ACTION_FAILE,
  GET_CATEGORYNUMBER_ACTION,
  GET_CATEGORYNUMBER_ACTION_SUCCESS,
  GET_CATEGORYNUMBER_ACTION_FAILE,
  GET_RANGENUMBER_ACTION,
  GET_RANGENUMBER_ACTION_SUCCESS,
  GET_RANGENUMBER_ACTION_FAILE,
  SEND_SUBLINE_ACTION,
  SEND_SUBLINE_ACTION_SUCCESS,
  SEND_SUBLINE_ACTION_FAILE,
  SEND_SUBCATEGORYSYNC_ACTION,
  SEND_SUBCATEGORYSYNC_ACTION_SUCCESS,
  SEND_SUBCATEGORYSYNC_ACTION_FAILE,
  SEND_ADD_SUBNAME_ACTION,
  SEND_ADD_SUBNAME_ACTION_SUCCESS,
  SEND_ADD_SUBNAME_ACTION_FAILE,
  GET_SUBLINERS_ACTION,
  GET_SUBLINERS_ACTION_SUCCESS,
  GET_SUBLINERS_ACTION_FAILE,
} from './constants';

export function setTariffAction(params) {
  return {
    type: SET_TARRIFS,
    params,
  };
}
export function setTariffSuccessAction(data) {
  return {
    type: SET_TARRIFS_SUCCESS,
    data,
  };
}
export function setTariffFaileAction(error) {
  return {
    type: SET_TARRIFS_FAILE,
    error,
  };
}

export function getTariffAction(params) {
  return {
    type: GET_TARRIFS,
    params,
  };
}
export function getTariffSuccessAction(data) {
  return {
    type: GET_TARRIFS_SUCCESS,
    data,
  };
}
export function getTariffFaileAction(error) {
  return {
    type: GET_TARRIFS_FAILE,
    error,
  };
}

export function getSubNumberAction() {
  return {
    type: GET_SUBNUMBER_TARRIFS,
  };
}
export function getSubNumberSuccessAction(data) {
  return {
    type: GET_SUBNUMBER_TARRIFS_SUCCESS,
    data,
  };
}
export function getSubNumberFailAction(error) {
  return {
    type: GET_SUBNUMBER_TARRIFS_FAILE,
    error,
  };
}

export function getPriceAction() {
  return {
    type: GET_PRICE_ACTION,
  };
}
export function getPriceSuccessAction(data) {
  return {
    type: GET_PRICE_ACTION_SUCCESS,
    data,
  };
}
export function getPriceFailAction(error) {
  return {
    type: GET_PRICE_ACTION_FAILE,
    error,
  };
}

// #region get SubName
export function getSubNameAction() {
  return {
    type: GET_SUBNAME_ACTION,
  };
}
export function getSubNameSuccessAction(data) {
  return {
    type: GET_SUBNAME_ACTION_SUCCESS,
    data,
  };
}
export function getSubNameFailAction(error) {
  return {
    type: GET_SUBNAME_ACTION_FAILE,
    error,
  };
}
// #endregion get SubName
// #region get CategoryNumber
export function getCategoryNumberAction() {
  return {
    type: GET_CATEGORYNUMBER_ACTION,
  };
}
export function getCategoryNumberSuccessAction(data) {
  return {
    type: GET_CATEGORYNUMBER_ACTION_SUCCESS,
    data,
  };
}
export function getCategoryNumberFailAction(error) {
  return {
    type: GET_CATEGORYNUMBER_ACTION_FAILE,
    error,
  };
}
// #endregion get CategoryNumber
// #region get RANGENUMBER
export function getRangeNumberAction() {
  return {
    type: GET_RANGENUMBER_ACTION,
  };
}
export function getRangeNumberSuccessAction(data) {
  return {
    type: GET_RANGENUMBER_ACTION_SUCCESS,
    data,
  };
}
export function getRangeNumberFailAction(error) {
  return {
    type: GET_RANGENUMBER_ACTION_FAILE,
    error,
  };
}

// #endregion get RANGENUMBER

// #region SEND SubLine
export function sendSubLineAction(params) {
  return {
    type: SEND_SUBLINE_ACTION,
    params,
  };
}
export function sendSubLineSuccessAction(data) {
  return {
    type: SEND_SUBLINE_ACTION_SUCCESS,
    data,
  };
}
export function sendSubLineFailAction(error) {
  return {
    type: SEND_SUBLINE_ACTION_FAILE,
    error,
  };
}
// #endregion SEND SubLine
// #region SEND SyncSubCategory
export function sendSyncSubCategoryAction(params) {
  return {
    type: SEND_SUBCATEGORYSYNC_ACTION,
    params,
  };
}
export function sendSyncSubCategorySuccessAction(data) {
  return {
    type: SEND_SUBCATEGORYSYNC_ACTION_SUCCESS,
    data,
  };
}
export function sendSyncSubCategoryFailAction(error) {
  return {
    type: SEND_SUBCATEGORYSYNC_ACTION_FAILE,
    error,
  };
}
// #endregion SEND SyncSubCategory
// #region SEND ADD_SUBNAME
export function addSubNameSubLineAction(params) {
  return {
    type: SEND_ADD_SUBNAME_ACTION,
    params,
  };
}
export function addSubNameSubLineSuccessAction(data) {
  return {
    type: SEND_ADD_SUBNAME_ACTION_SUCCESS,
    data,
  };
}
export function addSubNameSubLineFailAction(error) {
  return {
    type: SEND_ADD_SUBNAME_ACTION_FAILE,
    error,
  };
}
// #endregion SEND ADD_SUBNAME
// #region getSubLinersActions
export function getSubLinersActions(params) {
  return {
    type: GET_SUBLINERS_ACTION,
    params,
  };
}
export function getSubLinersSuccessAction(data) {
  return {
    type: GET_SUBLINERS_ACTION_SUCCESS,
    data,
  };
}
export function getSubLinersFailAction(error) {
  return {
    type: GET_SUBLINERS_ACTION_FAILE,
    error,
  };
}
// #endregion getSubLinersActions
