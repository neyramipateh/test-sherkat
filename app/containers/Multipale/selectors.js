import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the multipale state domain
 */

const selectMultipaleDomain = state => state.multipale || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Multipale
 */

export const makeSelectMultipale = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate,
  );

export const makeSelectTariffsMultipale = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate.tariff.data,
  );

export const makeGetSubNumberMultipale = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate.getsubnumber.data,
  );
export const makeGetpriceMultipale = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate.getprice.data,
  );
export const makeGetSubNameMultipale = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate.getsubname.data,
  );
export const makeGetCategoryNumberMultipale = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate.getcategorynumber.data,
  );
export const makeGetRangeNumberMultipale = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate.getrange.data,
  );
export const makeSelectSubNameSubLiners = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate.addsubname.data,
  );
export const makeSelectGetSubLinersSelector = () =>
  createSelector(
    selectMultipaleDomain,
    substate => substate.getsubliner.data,
  );
