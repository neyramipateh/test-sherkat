import styled from 'styled-components';
import Dialog from '@material-ui/core/Dialog';

export const Div = styled.div`
  display: grid;
  .title-tariff {
    text-align: center;
    border-bottom: 1px solid #665665;
    font-size: 18px;
  }
  .wraper-tarffe {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
    top: 30px;
    position: relative;
    gap: 16%;
    -webkit-box-align: center;
    align-items: center;
    align-content: center;
    margin: auto;
    width: auto;
    height: fit-content;
    padding: 5%;
    max-width: 900px;
    min-width: 100%;
  }
  .tarrif-price {
    display: grid;
    margin: auto;
  }
  .tarrif-name {
    display: grid;
    margin: auto;
  }
  .btn-tarrif {
    align-items: center;
    display: grid;
    place-content: center;
    top: 100px;
    margin: auto;
    color: azure;
    padding: 4%;
    position: inherit;
  }
  .btn-success {
    color: #fff;
    background-color: #ee5544;
  }
`;

export const WrapperSubNumber = styled.div`
  & .formControl {
    width: 100%;
  }

  & label + .MuiInput-formControl {
    margin-top: 0;
  }

  & .MuiInput-underline:after,
  & .MuiInput-underline:before {
    display: none;
  }

  & .MuiSelect-selectMenu {
    border: 1px solid #c4c4c4;
    border-radius: 4px;
    background: #fff;
    padding: 12px;
  }

  & .chips {
    display: flex;
    flex-direction: row;
    flex-flow: wrap;
  }

  & .chip {
    width: auto;
    background: #df1051;
    margin: 2px;

    & .MuiChip-deleteIcon {
      margin: 0;
      margin-left: 5px;
      color: #fff;
    }
  }

  & .chip-empty {
    width: auto;
    font-weight: bold;
    padding: 6px;
    display: inline-block;
  }

  & .searchTag {
    background: red;
  }
`;

export const UlStyles = styled.div`
  .ul-tsub-head {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
    border-bottom: 1px solid #e0e0e0;
    list-style-type: none;
    direction: rtl;
    font-family: MONOSPACE, Tahoma;
    font-size: x-large;
    color: black;
    text-align: -webkit-center;
  }
  .ul-tsub {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
    border-bottom: 1px solid #e0e0e0;
    list-style-type: none;
    direction: rtl;
    font-family: system-ui;
    color: #fffffe;
    background-color: #2e4c4c;
    font-size: 14px;
  }
`;
export const StylesWraperTariffes = styled.div`
  .li-tsub {
    text-align: center;
  }
`;

export const StyleDialogCategory = styled(Dialog)`
  width: 100%;
  display: grid;
  .ul-subcategory {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
    list-style-type: none;
    text-align: center;
    font-family: inherit;
    border-bottom: 1px solid #dee3da;
    border-left: 1px solid #d7d7cf;
    border-block: 1px solid #665666;
    padding-inline: 9px;
    font-size: unset;
  }

  .wraper-zarib: {
    display: grid;
    text-align: center;
    gap: 5%;
    margin: 20px;
    align-content: space-between;
    justify-content: space-evenly;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
  }

  & .MuiDialog-paperWidthSm {
    width: 100%;
  }
`;

export const WraperZaribStyles = styled.div`
  display: grid;
  text-align: center;
  gap: 5%;
  margin: 20px;
  align-content: space-between;
  justify-content: space-evenly;
  grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
  .btn-wraper {
    display: grid;
    text-align: center;
    gap: 5%;
    margin: 20px;
    align-content: space-between;
    justify-content: space-evenly;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
  }
`;

export const SubLineStyles = styled.div`
  .wraper-subline {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
    gap: 10%;
    justify-content: space-between;
    margin: auto;
    width: 100%;
    padding: 10%;
    align-items: center;
    text-align: center;
  }
  .select-operate {
    width: 100%;
    margin: auto;
    display: grid;
  }
`;
