/* eslint-disable no-restricted-globals */
/**
 *
 * Multipale
 *
 */

import { Grid } from '@material-ui/core';
import Loading from 'components/Loading';
import LoadingWithText from 'components/LoadingWithText/index';
import PropTypes from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import DashboardLayout from '../../layouts/DashboardLayout/index';
import { getPriceAction, getSubNumberAction } from './actions';
import reducer from './reducer';
import saga from './saga';
import { makeGetpriceMultipale } from './selectors';
import { getSub } from './setLocalSubnumber/setLocalSubnumber';
import { Div, UlStyles } from './styles';
import { WraperAllTableStyles } from './SubNumbers/styles';
import Tariffes from './Tariffes';
import TariffeSub from './tariffeSub/TariffeSub';
import SubCategoryLine from './Zaraeb/Loadable';

export function Multipale({
  getSubNUmberFromServer,
  getPriceFromServer,
  price,
}) {
  useInjectReducer({ key: 'multipale', reducer });
  useInjectSaga({ key: 'multipale', saga });
  const [selectedProduct, setSelectedProduct] = useState(null);
  const sub = getSub();
  const isSub = !!sub;

  useEffect(() => {
    getSubNUmberFromServer();
    getPriceFromServer();
  }, [isSub]);

  return (
    <DashboardLayout>
      <Div>
        {isSub === true ? (
          <div>
            <Tariffes />
          </div>
        ) : (
          <LoadingWithText />
        )}
      </Div>
      {price ? (
        <div>
          <WraperAllTableStyles>
            <UlStyles>
              <ul className="ul-tsub-head">
                <li>id</li>
                <li>name</li>
                <li>price</li>
                <li>phone</li>
                <li>Operators</li>
              </ul>
              {price.data ? (
                Object.values(price.data).map(subitems => (
                  <TariffeSub
                    key={subitems.id}
                    items={subitems}
                    handleClikedProduct={() => setSelectedProduct(subitems)}
                  />
                ))
              ) : (
                <h5>
                  <LoadingWithText />
                </h5>
              )}
            </UlStyles>
          </WraperAllTableStyles>

          <Grid>
            {selectedProduct && (
              <SubCategoryLine
                product={selectedProduct}
                onClose={() => setSelectedProduct(null)}
              />
            )}
          </Grid>
        </div>
      ) : (
        <Loading />
      )}
    </DashboardLayout>
  );
}

Multipale.propTypes = {
  price: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  getSubNUmberFromServer: PropTypes.func,
  getPriceFromServer: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  price: makeGetpriceMultipale(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPriceFromServer: () => dispatch(getPriceAction()),
    getSubNUmberFromServer: () => dispatch(getSubNumberAction()),
  };
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Multipale);
