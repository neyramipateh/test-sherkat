/**
 *
 * Asynchronously loads the component for Multipale
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
