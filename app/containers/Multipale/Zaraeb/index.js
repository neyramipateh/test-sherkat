/* eslint-disable camelcase */
import { Button, TextField } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { sendSyncSubCategoryAction } from '../actions';
import { StyleDialogCategory, WraperZaribStyles } from '../styles';

const SubCategoryLine = ({ onClose, product, dispatch }) => {
  const [priceData, setPriceData] = useState({
    persian: 0,
    latin: 0,
    irancell: 0,
    mci: 0,
  });
  const { sub_category } = product;
  const [productData, setProductData] = useState(sub_category || 0);
  const [price, setPrice] = useState({ prices: null });

  // const isUpdated =
  //   product.persian !== productData.persian ||
  //   product.latin !== productData.latin ||
  //   product.irancell !== productData.irancell ||
  //   product.mci !== productData.mci;

  const handleChangeData = e => {
    let key = e.currentTarget.id;
    let { value } = e.currentTarget;
    if ('value' in e.target) {
      key = key || e.target.name;
      // eslint-disable-next-line prefer-destructuring
      value = e.target.value;
    }
    handleSetUpdate(key, value);
  };

  const handleUpdateToServer = productvals => {
    const SendData = Object.assign(
      {
        category_id: productvals.sub_category_id,
        id: productvals.id,
        subnumber: productvals.sub_number_id,
      },
      { ...priceData },
    );
    dispatch(sendSyncSubCategoryAction(SendData));
  };
  console.log(productData, priceData);

  const handleSetUpdate = (key, value) => {
    setProductData({ ...productData, [key]: value });

    setPriceData({ ...priceData, [key]: value * product.price });
    const prices = Object.values(priceData).reduce(
      (previousValue, current) => previousValue + current,
    );
    setPrice({ ...price, prices: parseFloat(Math.round(prices)) });
  };

  return (
    <StyleDialogCategory open>
      <WraperZaribStyles>
        <div>
          <h1>{product.price}</h1>
          <hr />
          <h1>{price.prices}</h1>
          <hr />

          <div />
          <TextField
            label="فارسی"
            name="persian"
            id="persian"
            value={productData.persian || 0}
            onChange={handleChangeData}
          />
          <h5>{priceData.persian}</h5>
          <TextField
            label="لاتین"
            name="latin"
            id="latin"
            value={productData.latin || 0}
            onChange={handleChangeData}
          />
          <h5>{priceData.latin}</h5>
        </div>
        <div>
          <TextField
            label="ایرانسل"
            name="irancell"
            id="irancell"
            value={productData.irancell || 0}
            onChange={handleChangeData}
          />
          <h5>{priceData.irancell}</h5>
          <TextField
            label="همراه اول"
            name="mci"
            id="mci"
            value={productData.mci || 0}
            onChange={handleChangeData}
          />
          <h5>{priceData.mci}</h5>
        </div>
      </WraperZaribStyles>
      <div className="btn-wraper">
        <Button onClick={onClose}>بستن</Button>
        <Button onClick={() => handleUpdateToServer(product)}>ذخیره</Button>
      </div>
    </StyleDialogCategory>
  );
};

SubCategoryLine.propTypes = {
  onClose: PropTypes.func,
  product: PropTypes.object,
  dispatch: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SubCategoryLine);
