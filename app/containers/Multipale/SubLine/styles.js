const { default: styled } = require('styled-components');

export const SubNameStyles = styled.div`
  width: 100%;
  & .form {
    width: 100%;
  }
  & .sub-form {
    font-family: yara;
  }
`;
export const SubNumberStyles = styled.div`
  width: 100%;
  & .form {
    width: 100%;
  }
`;

export const AddSubNameStyles = styled.div`
  align-items: center;
  text-align: center;
  margin: auto;
  .wraper {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
    gap: 5%;
    font-family: cursive;
    margin: auto;
    direction: rtl;
    align-items: center;
  }
  .add-subname {
  }
  .sub-btn {
  }
  .btn-subname {
    color: blueviolet;
    background-color: black;
  }
`;

export const SubLinersStyles = styled.div``;
