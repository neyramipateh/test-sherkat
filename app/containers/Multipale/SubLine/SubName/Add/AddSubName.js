import { Button, TextField } from '@material-ui/core';
import NotificationBox from 'components/NotificationBox';
import { addSubNameSubLineAction } from 'containers/Multipale/actions';
import {
  makeSelectGetSubLinersSelector,
  makeSelectSubNameSubLiners,
} from 'containers/Multipale/selectors';
import PropTypes from 'prop-types';
import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { AddSubNameStyles } from '../../styles';
import SubLiners from '../../SubLiners';

function AddSubName({ sendSubNameFromServer, dataItems }) {
  const [subnames, setSubNames] = useState({ title: null });
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const handleSetSubNameSubLiners = (key, value) => {
    setSubNames({ ...subnames, [key]: value });
  };

  const handleClickeToSendPropsServer = () => {
    sendSubNameFromServer(subnames);
  };

  function handlePageChange(p, s) {
    setPage(p);
    setPageSize(s);
  }

  return (
    <AddSubNameStyles>
      <div>
        <NotificationBox />
      </div>
      <h1>افزودن نام خطوط</h1>
      <div className="wraper">
        <div className="add-subname">
          <TextField
            id="outlined-basic"
            label="نام خطوط"
            variant="outlined"
            onChange={e => handleSetSubNameSubLiners('title', e.target.value)}
          />
        </div>
        <div className="sub-btn">
          <Button
            type="button"
            label="Send"
            className="btn-subname"
            onClick={handleClickeToSendPropsServer}
          >
            Sending
          </Button>
        </div>
      </div>
      <div>
        {dataItems && (
          <SubLiners
            data={dataItems.data}
            total={dataItems.total}
            onChangePage={handlePageChange}
            page={page}
            size={pageSize}
          />
        )}
      </div>
    </AddSubNameStyles>
  );
}

AddSubName.propTypes = {
  dataItems: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  // value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  sendSubNameFromServer: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  datas: makeSelectSubNameSubLiners(),
  dataItems: makeSelectGetSubLinersSelector(),
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    sendSubNameFromServer: params => dispatch(addSubNameSubLineAction(params)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddSubName);
