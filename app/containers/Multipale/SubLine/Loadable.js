/**
 *
 * Asynchronously loads the component for SubLine
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
