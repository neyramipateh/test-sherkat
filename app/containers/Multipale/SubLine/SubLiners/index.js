import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
} from '@material-ui/core';
import { getSubLinersActions } from 'containers/Multipale/actions';
import PropTypes from 'prop-types';
import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { SubLinersStyles } from '../styles';
const columns = [
  { name: 'id', title: 'واحد' },
  { name: 'price', title: ' قیمت' },

  {
    name: 'sub_name',
    title: 'نام',
    cast: v => (v ? Object.values(v.title) : null),
  },
  {
    name: 'range_number',
    title: 'تعداد شماره',
    cast: v => (v ? v.numbers : null),
  },
  {
    name: 'sub_category',
    title: 'تایپ خطوط',
    cast: v => (v ? Object.values(v.type) : null),
  },
  {
    name: 'sub_number',
    title: 'اپراتور',
    cast: v => (v ? Object.values(v.title) : null),
  },
];

function SubLiners({ data, dispatch, total, onChangePage, page, size }) {
  function handleChangePage(e, newPage) {
    onChangePage(newPage + 1, size);
  }

  function handleChangeRowsPerPage(e) {
    onChangePage(1, parseInt(e.target.value, 10));
  }

  useEffect(() => {
    dispatch(getSubLinersActions({ page, size }));
  }, []);
  return (
    <SubLinersStyles>
      <Grid>
        <Table>
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  className="padd-inputs"
                  key={column.name}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.title}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data &&
              data.map(product => (
                <TableRow hover role="checkbox" tabIndex={-1} key={product.id}>
                  {columns.map(column => {
                    const value = product[column.name];
                    return (
                      <TableCell
                        className={column.className}
                        key={column.name}
                        align={column.align}
                        dir={column.dir}
                      >
                        {column.cast ? column.cast(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </Grid>
      {data && (
        <TablePagination
          rowsPerPageOptions={[10, 25, 50]}
          component="div"
          count={total || 0}
          rowsPerPage={size}
          page={page - 1}
          labelRowsPerPage=""
          labelDisplayedRows={() => `صفحه ${page}`}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      )}
    </SubLinersStyles>
  );
}
SubLiners.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onChangePage: PropTypes.func,
  page: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  total: PropTypes.number,
};

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SubLiners);
