/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import {
  FormControl,
  Input,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core';
import LoadingWithText from 'components/LoadingWithText';
import { getCategoryNumberAction } from 'containers/Multipale/actions';
import PropTypes from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { SubNameStyles } from '../styles';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
function CategoryNumber({ dataItem, handleChange, dispatch }) {
  const [category, setCategory] = useState();
  const { data } = dataItem;
  const handleSetChange = val => {
    if (val) {
      setCategory(val);
      handleChange(val);
    }
  };
  useEffect(() => {
    dispatch(getCategoryNumberAction());
  }, []);
  return (
    <SubNameStyles>
      {data ? (
        <FormControl className="form">
          <InputLabel id="category" className="sub-form">
            نام دسته بندی
          </InputLabel>
          <Select
            id="category"
            value={category || ''}
            onChange={e => handleSetChange(e.target.value)}
            input={<Input id="category-id" />}
            MenuProps={MenuProps}
          >
            {data ? (
              Object.values(data).map(name => (
                <MenuItem key={name.id} value={name.id}>
                  {name.type}
                </MenuItem>
              ))
            ) : (
              <div className="loading-data">
                <LoadingWithText />
              </div>
            )}
          </Select>
        </FormControl>
      ) : (
        <LoadingWithText />
      )}
    </SubNameStyles>
  );
}

CategoryNumber.propTypes = {
  dataItem: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  handleChange: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  // handleChange: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  // multipale: makeSelectTariffsMultipale(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(CategoryNumber);
