/**
 *
 * SubLine
 *
 */

import {
  Button,
  InputAdornment,
  TextField,
  Container,
  Tabs,
  Paper,
  Tab,
} from '@material-ui/core';

import { EditAttributes } from '@material-ui/icons';
import LoadingWithText from 'components/LoadingWithText';
import PropTypes from 'prop-types';
import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { getAuth } from 'utils/auth';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import DashboardLayout from '../../../layouts/DashboardLayout/Loadable';
import { getSubNumberAction, sendSubLineAction } from '../actions';
import reducer from '../reducer';
import saga from '../saga';
import {
  makeGetCategoryNumberMultipale,
  makeGetRangeNumberMultipale,
  makeGetSubNameMultipale,
  makeGetSubNumberMultipale,
} from '../selectors';
import { SubLineStyles } from '../styles';
import SubNumbers from '../SubNumbers';
import CategoryNumber from './CategoryNumber';
import RangeNumber from './RangeNumber';
import AddSubName from './SubName/Add/AddSubName';
import SubName from './SubName/index';
function SubLine({ subname, subnumber, dispatch, category, ranges }) {
  useInjectReducer({ key: 'multipale', reducer });
  useInjectSaga({ key: 'multipale', saga });
  const [subLine, setSubLine] = useState({
    subnames: null,
    subnumbers: null,
    category: null,
    range: null,
    price: null,
  });
  // const issubnumber = !!subnumber;
  const [selectedTabs, setSelectedTabs] = useState(0);
  const handleSetSubLine = (key, value) => {
    setSubLine({ ...subLine, [key]: value });
  };
  const handleChangeSendTOServer = () => {
    dispatch(sendSubLineAction({ ...subLine }));
  };
  const auth = getAuth();
  return (
    <DashboardLayout>
      {auth && (
        <Container>
          <Tabs
            component={Paper}
            value={selectedTabs}
            onChange={(e, tabIndex) => {
              setSelectedTabs(tabIndex);
            }}
            indicatorColor="primary"
            textColor="primary"
            className="tabs"
          >
            <Tab label="تعرفه خطوط " />
            <Tab label="افزودن نام خطوط" />
          </Tabs>
          <div className="setting-in">
            {selectedTabs === 0 && (
              <SubLineStyles>
                <div className="wraper-subline">
                  <div className="select-operate">
                    {subname ? (
                      <SubName
                        dataItem={subname.data}
                        defaultValue={subLine.subname}
                        handleChange={defaultValue =>
                          handleSetSubLine('subnames', defaultValue)
                        }
                      />
                    ) : (
                      <LoadingWithText />
                    )}
                  </div>
                  <div className="select-operate">
                    {subnumber ? (
                      <SubNumbers
                        dataItem={subnumber.data}
                        defaultValue={subLine.subnumber}
                        handleChange={defaultValue =>
                          handleSetSubLine('subnumbers', defaultValue)
                        }
                      />
                    ) : (
                      <LoadingWithText />
                    )}
                  </div>
                </div>
                <div className="wraper-subline">
                  <div className="select-operate">
                    {category ? (
                      <CategoryNumber
                        dataItem={category || ''}
                        value={subLine.category}
                        handleChange={value =>
                          handleSetSubLine('category', value)
                        }
                      />
                    ) : (
                      <LoadingWithText />
                    )}
                  </div>
                  <div className="select-operate">
                    {ranges ? (
                      <RangeNumber
                        dataItem={ranges.data}
                        value={subLine.range || ''}
                        handleChange={value => handleSetSubLine('range', value)}
                      />
                    ) : (
                      <LoadingWithText />
                    )}
                  </div>
                </div>
                <div className="wraper-subline">
                  <div className="tarrif-price">
                    <TextField
                      required
                      variant="outlined"
                      label="قیمت پایه تعرفه"
                      name="price"
                      value={subLine.price || ''}
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <EditAttributes />
                          </InputAdornment>
                        ),
                      }}
                      onChange={e =>
                        handleSetSubLine('price', e.target.value.trim())
                      }
                    />
                  </div>
                  <Button
                    className="btn-subline"
                    type="button"
                    color="secondary"
                    variant="outlined"
                    onClick={handleChangeSendTOServer}
                  >
                    Send
                  </Button>
                </div>
              </SubLineStyles>
            )}
          </div>
          <div className="setting-in">
            {selectedTabs === 1 && <AddSubName />}
          </div>
        </Container>
      )}
    </DashboardLayout>
  );
}

SubLine.propTypes = {
  subname: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  subnumber: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  category: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  ranges: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  // value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  // handleChange: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  subname: makeGetSubNameMultipale(),
  subnumber: makeGetSubNumberMultipale(),
  category: makeGetCategoryNumberMultipale(),
  ranges: makeGetRangeNumberMultipale(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getSubNUmberFromServer: () => dispatch(getSubNumberAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SubLine);
