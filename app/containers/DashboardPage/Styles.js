import styled from 'styled-components';
export const DashboardStyles = styled.div`
  & .tabs {
    flex-direction: row-reverse;
    justify-content: flex-start;
    display: grid;
    direction: rtl;
  }
`;

export const SocialStyles = styled.div`
  background-color: #fff;
  position: relative;
  margin: 1rem;

  .social-wraper {
    width: 100%;
    display: grid;
    position: absolute;
    grid-template-columns: repeat(auto-fill, minmax(330px, 3fr));
    direction: ltr;
    flex-direction: row-reverse;
    grid-gap: 1rem;
    row-gap: 1rem;
    column-gap: 1rem;
    border: 1px solid #5555660d;
    border-radius: 10px;
    margin: 1rem auto;
    padding: 1rem;
    height: auto;
  }
  .social-media {
    width: 100%;
  }
  .setting-li {
    width: 100%;
  }
`;
