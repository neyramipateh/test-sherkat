/**
 *
 * DashboardPage
 *
 */

import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Helmet } from 'react-helmet';
import DashboardLayout from 'layouts/DashboardLayout';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Container, Tabs, Paper, Tab } from '@material-ui/core';
import { push } from 'connected-react-router';
import { LOGIN_ROUTE } from 'containers/App/routes';
import GlyMaps from './Map/index';
import makeSelectDashboardPage, { makeSelectSendSetting } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { getAuth, isAdminUser } from '../../utils/auth';

import NotificationBox from '../../components/NotificationBox';
import { DashboardStyles } from './Styles';

const auth = getAuth();

export function DashboardPage({ dispatch }) {
  useInjectReducer({ key: 'dashboardPage', reducer });
  useInjectSaga({ key: 'dashboardPage', saga });
  const [selectedTabs, setSelectedTabs] = useState(0);
  const isAdmin = isAdminUser();
  return (
    <div>
      {auth ? (
        <div>
          <Helmet>
            <title>داشبورد</title>
            <meta name="description" content="صفحه داشبورد" />
          </Helmet>

          <DashboardLayout fullWidth>
            <NotificationBox />
            <DashboardStyles>
              {isAdmin && (
                <Container>
                  <Tabs
                    component={Paper}
                    value={selectedTabs}
                    onChange={(e, tabIndex) => {
                      setSelectedTabs(tabIndex);
                    }}
                    indicatorColor="primary"
                    textColor="primary"
                    className="tabs"
                  >
                    <Tab label="تنظیمات سایت " />
                    <Tab label="بخش سوشیال" />
                  </Tabs>
                  <div className="setting-in">
                    <GlyMaps />
                  </div>
                  <div className="setting-in">
                    {/* {selectedTabs === 1 && <Socials />} */}
                  </div>
                </Container>
              )}
            </DashboardStyles>
          </DashboardLayout>
        </div>
      ) : (
        dispatch(push(LOGIN_ROUTE))
      )}
    </div>
  );
}

DashboardPage.propTypes = {
  // settingData: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  dashboardPage: makeSelectDashboardPage(),
  settingData: makeSelectSendSetting(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(DashboardPage);
