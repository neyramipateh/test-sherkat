/*
 *
 * DashboardPage actions
 *
 */

import {
  SEND_DATA_SETTING,
  SEND_DATA_SETTING_FAIL,
  SEND_DATA_SETTING_SUCCESS,
  SET_TARRIFS,
  SET_TARRIFS_SUCCESS,
  SET_TARRIFS_FAILE,
} from './constants';

export function sendSettingAction(params) {
  return {
    type: SEND_DATA_SETTING,
    params,
  };
}
export function sendSettingSuccessAction(data) {
  return {
    type: SEND_DATA_SETTING_SUCCESS,
    data,
  };
}
export function sendSettingFailAction(error) {
  return {
    type: SEND_DATA_SETTING_FAIL,
    error,
  };
}


