import { setTarifApi } from 'api/tariffe';
import { call, put, takeLatest } from 'redux-saga/effects';
import { setSettingDataApi } from '../../api/settingApi';
import {
  NOTIFICATION_TYPE_ERROR,
  NOTIFICATION_TYPE_SUCCESS,
} from '../../components/NotificationBox';
import { notificationShowAction } from '../App/actions';
import {
  sendSettingFailAction,
  sendSettingSuccessAction,
  setTariffFaileAction,
  setTariffSuccessAction,
} from './actions';
import { SEND_DATA_SETTING, SET_TARRIFS } from './constants';

function* sendSettings({ params }) {
  try {
    const response = yield call(setSettingDataApi, params);
    yield put(sendSettingSuccessAction(response.data));

    yield put(
      notificationShowAction(
        'به روز رسانی محصولات با موفقیت انجام شد',
        NOTIFICATION_TYPE_SUCCESS,
      ),
    );
  } catch (error) {
    yield put(
      notificationShowAction(
        'به روز رسانی اطلاعات کاربری با خطا مواجه شد',
        NOTIFICATION_TYPE_ERROR,
      ),
    );
    yield put(sendSettingFailAction(error.response));
  }
}

// Individual exports for testing
export default function* dashboardPageSaga() {
  yield takeLatest(SEND_DATA_SETTING, sendSettings);
  // See example in containers/HomePage/saga.js
}
