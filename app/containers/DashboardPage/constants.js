/*
 *
 * DashboardPage constants
 *
 */

export const SEND_DATA_SETTING = 'app/DashboardPage/SEND_DATA_SETTING';
export const SEND_DATA_SETTING_SUCCESS =
  'app/DashboardPage/SEND_DATA_SETTING_SUCCESS';
export const SEND_DATA_SETTING_FAIL =
  'app/DashboardPage/SEND_DATA_SETTING_FAIL';

