/*
 *
 * DashboardPage reducer
 *
 */
import produce from 'immer';
import {
  SEND_DATA_SETTING,
  SEND_DATA_SETTING_FAIL,
  SEND_DATA_SETTING_SUCCESS,
  SET_TARRIFS,
  SET_TARRIFS_FAILE,
  SET_TARRIFS_SUCCESS,
} from './constants';

export const initialState = {
  sendsetting: {
    params: null,
    data: null,
    error: null,
  },
  tariff: {
    params: null,
    data: null,
    error: null,
  },
};

/* eslint-disable default-case, no-param-reassign */
const dashboardPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SEND_DATA_SETTING:
        draft.sendsetting.params = action.params;
        draft.sendsetting.error = null;
        draft.sendsetting.data = null;
        break;
      case SEND_DATA_SETTING_SUCCESS:
        draft.sendsetting.params = null;
        draft.sendsetting.data = action.data;
        draft.sendsetting.error = null;
        break;
      case SEND_DATA_SETTING_FAIL:
        draft.sendsetting.params = null;
        draft.sendsetting.data = null;
        draft.sendsetting.error = action.error;
        break;
    }
  });

export default dashboardPageReducer;
