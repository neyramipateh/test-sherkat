export function setDataSetting(settingData) {
  if (settingData) {
    localStorage.setItem('Setting', JSON.stringify(settingData));
  } else {
    localStorage.removeItem('Setting');
  }
}

export function getSetting() {
  try {
    return JSON.parse(localStorage.getItem('Setting'));
  } catch (error) {
    // nothing
  }

  return null;
}
