import { Button, TextField } from '@material-ui/core/';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import LoadingWithText from 'components/LoadingWithText';
import PropTypes from 'prop-types';
import React, { memo, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeGetpriceDataMultipale } from '../selectors';
import { CreatePanelStyles } from '../styles';
import PlanTarefe from './TareffePlane/index';


import { makeSelectGetSubLinersSelector } from '../../Multipale/selectors';
import SubLiners from './TareffePlane/SubLiners';
function CreatePanel({ price, dataItems }) {
  const [panel, setPanel] = useState({
    name: '',
    price: '',
    date: '',
    gift: '',
    tarffeh: '',
  });

  const [checked, setChecked] = useState(false);
  const client = checked === true ? 'user' : 'delegate';
  const dataCheck = checked === true ? '#666' : '#fff';
  const dataChecks = checked !== true ? '#666' : '#fff';

  const toggleChecked = () => {
    setPanel({ ...panel, tarffeh: '' });
    setChecked(prev => !prev);
  };
  console.log(dataItems);
  const dataItem = price
    ? Object.values(price).filter(item => item.userType === client)
    : null;
  const handleChange = (key, value) => {
    setPanel({ ...panel, [key]: value });
  };

  useEffect(() => {
    console.log(dataItems);
  });

  return (
    <CreatePanelStyles>
      <div className="wraper-panel">
        <ButtonGroup>
          <Button
            disabled={!checked}
            aria-label="reduce"
            onClick={toggleChecked}
            style={{ backgroundColor: dataCheck }}
            className="disabled"
          >
            <h5>کاربر</h5>
          </Button>
          <Button
            aria-label="increase"
            disabled={checked}
            onClick={toggleChecked}
            className="disables"
            style={{ backgroundColor: dataChecks }}
          >
            <h5>نماینده</h5>
          </Button>
        </ButtonGroup>
      </div>
      <div className="wraper-panel">
        <TextField
          className="txt-panle"
          required
          id="outlined-required"
          label="نام پنل"
          type="text"
          value={panel.name}
          onChange={e => handleChange('name', e.target.value)}
        />
        <TextField
          required
          type="number"
          className="txt-panle"
          id="outlined-required"
          label="مدت زمان"
          value={panel.date}
          onChange={e => handleChange('date', e.target.value)}
        />
      </div>
      <div className="wraper-panel">
        <TextField
          className="txt-panle"
          required
          id="outlined-required"
          label="قیمت پنل"
          type="text"
          value={panel.price}
          onChange={e => handleChange('price', e.target.value)}
        />
        <TextField
          required
          type="float"
          className="txt-panle"
          id="outlined-required"
          label="اعتبار هدیه"
          value={panel.gift}
          onChange={e => handleChange('gift', e.target.value)}
        />
      </div>
      <div className="wraper-panel">
        <div>
          {price ? (
            <PlanTarefe
              panels={panel.tarffeh}
              data={dataItem}
              handleChange={handleChange}
            />
          ) : (
            <LoadingWithText />
          )}
        </div>
        <div>
          <SubLiners dataItem={dataItems} />
        </div>
      </div>
      <Button className="btn-panel" variant="outlined" color="secondary">
        Send
      </Button>
    </CreatePanelStyles>
  );
}

CreatePanel.propTypes = {
  price: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  dataItems: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

const mapStateToProps = createStructuredSelector({
  price: makeGetpriceDataMultipale(),
  dataItems: makeSelectGetSubLinersSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPriceFromServer: () => dispatch(getPriceAction()),
  };
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(CreatePanel);
