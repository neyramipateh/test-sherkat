/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import {
  FormControl,
  Input,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core';
import LoadingWithText from 'components/LoadingWithText';
import { getPriceAction } from 'containers/PanelPages/actions';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { SubNameStyles } from '../styles';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
function PlanTarefe({ data, handleChange, panels }) {
  const handleSetChange = (key, val) => {
    console.log(val);
    if (val) {
      handleChange(key, val);
    }
  };

  return (
    <SubNameStyles>
      {data ? (
        <FormControl className="form">
          <InputLabel id="category" className="sub-form">
            نام دسته بندی
          </InputLabel>
          <Select
            multiline
            id="category"
            value={panels || ''}
            onChange={e => handleSetChange('tarffeh', e.target.value)}
            input={<Input id="category-id" />}
            MenuProps={MenuProps}
            // renderValue={clientFilters}
          >
            {data ? (
              data.map(name => (
                <MenuItem key={name.id} value={name.price}>
                  {name.price}
                </MenuItem>
              ))
            ) : (
              <div className="loading-data">
                <LoadingWithText />
              </div>
            )}
          </Select>
        </FormControl>
      ) : (
        <LoadingWithText />
      )}
    </SubNameStyles>
  );
}

PlanTarefe.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.number,
  ]),
  handleChange: PropTypes.func.isRequired,
  panels: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  // handleChange: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  // multipale: makeSelectTariffsMultipale(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPriceFromServer: () => dispatch(getPriceAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PlanTarefe);
