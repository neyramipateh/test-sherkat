/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import {
  FormControl,
  Input,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core';
import LoadingWithText from 'components/LoadingWithText';
import { getSubNumberAction } from 'containers/Multipale/actions';
import PropTypes from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { SubNameStyles } from '../../styles';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
function SubLiners({ dataItem, handleChange, dispatch }) {
  const [subnumber, setSubNumber] = useState();

  const handleSetChange = val => {
    if (val) {
      setSubNumber(val);
      handleChange(val);
    }
  };
  useEffect(() => {
    dispatch(getSubNumberAction());
  }, []);
  return (
    <SubNameStyles>
      {dataItem ? (
        <FormControl className="form">
          <InputLabel id="subnumber" className="sub-form">
            پیش شماره
          </InputLabel>
          <Select
            id="subnumber"
            value={subnumber || ''}
            onChange={e => handleSetChange(e.target.value)}
            input={<Input id="select-multiple-chip" />}
            MenuProps={MenuProps}
          >
            {dataItem ? (
              Object.values(dataItem).map(name => (
                <MenuItem key={name.id} value={name.id}>
                  {name.phone}
                </MenuItem>
              ))
            ) : (
              <div className="loading-data">
                <LoadingWithText />
              </div>
            )}
          </Select>
        </FormControl>
      ) : (
        <LoadingWithText />
      )}
    </SubNameStyles>
  );
}

SubLiners.propTypes = {
  dataItem: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  handleChange: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  // handleChange: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  // multipale: makeSelectTariffsMultipale(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SubLiners);
