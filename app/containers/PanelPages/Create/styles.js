const { default: styled } = require('styled-components');
export const SubNameStyles = styled.div`
  width: 100%;
  & .form {
    width: 100%;
  }
  & .sub-form {
    font-family: yara;
  }
`;
