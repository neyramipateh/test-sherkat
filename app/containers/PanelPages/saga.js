import { getPriceDataApi } from 'api/subnumber';
import { call, put, takeLatest } from 'redux-saga/effects';
import { getPriceFailAction, getPriceSuccessAction } from './actions';
import { GET_PRICE_ACTION } from './constants';

function* getPrice() {
  try {
    const responce = yield call(getPriceDataApi);
    yield put(getPriceSuccessAction(responce.data));
    // setPrice(responce.data);
  } catch (error) {
    yield put(getPriceFailAction(error.responce));
  }
}

// Individual exports for testing
export default function* panelPagesSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_PRICE_ACTION, getPrice);
}
