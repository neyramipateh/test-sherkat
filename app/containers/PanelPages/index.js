/**
 *
 * PanelPages
 *
 */

import { Paper, Tab, Tabs } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import DashboardLayout from '../../layouts/DashboardLayout/Loadable';
import reducer from './reducer';
import saga from './saga';
import makeSelectPanelPages from './selectors';
import { PanelStyles } from './styles';
import CreatePanel from './Create/index';
import ShowPanel from './Show/index';
import { getPriceAction } from './actions';
export function PanelPages({ getPriceFromServer }) {
  useInjectReducer({ key: 'panelPages', reducer });
  useInjectSaga({ key: 'panelPages', saga });

  const [selectedTabs, setSelectedTabs] = useState(0);
  useEffect(() => {
    getPriceFromServer();
  }, []);

  return (
    <DashboardLayout>
      <PanelStyles>
        <Tabs
          component={Paper}
          value={selectedTabs}
          onChange={(e, tabIndex) => {
            setSelectedTabs(tabIndex);
          }}
          indicatorColor="primary"
          textColor="primary"
          className="tabs"
        >
          <Tab label="لیست پنل ها" />
          <Tab label="ایجاد بسته" />
        </Tabs>
        <div>
          {selectedTabs === 1 && (
            <div>
              <CreatePanel />
            </div>
          )}
        </div>
        <div>
          {selectedTabs === 0 && (
            <div>
              <ShowPanel />
            </div>
          )}
        </div>
      </PanelStyles>
    </DashboardLayout>
  );
}

PanelPages.propTypes = {
  getPriceFromServer: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  panelPages: makeSelectPanelPages(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPriceFromServer: () => dispatch(getPriceAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PanelPages);
