/*
 *
 * PanelPages constants
 *
 */

export const DEFAULT_ACTION = 'app/PanelPages/DEFAULT_ACTION';
// #region get_PRICE_ACTION
export const GET_PRICE_ACTION = 'app/PanelPages/GET_PRICE_ACTION';
export const GET_PRICE_ACTION_SUCCESS =
  'app/PanelPages/GET_PRICE_ACTION_SUCCESS';
export const GET_PRICE_ACTION_FAILE = 'app/PanelPages/GET_PRICE_ACTION_FAILE';
// #region get_PRICE_ACTION
