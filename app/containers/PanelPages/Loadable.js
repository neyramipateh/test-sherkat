/**
 *
 * Asynchronously loads the component for PanelPages
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
