/*
 *
 * PanelPages reducer
 *
 */
import produce from 'immer';
import {
  GET_PRICE_ACTION,
  GET_PRICE_ACTION_SUCCESS,
  GET_PRICE_ACTION_FAILE,
} from './constants';

export const initialState = {
  getprice: {
    data: null,
    error: null,
  },
};

/* eslint-disable default-case, no-param-reassign */
const panelPagesReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_PRICE_ACTION:
        draft.getprice.error = null;
        break;
      case GET_PRICE_ACTION_SUCCESS:
        draft.getprice.data = action.data;

        break;
      case GET_PRICE_ACTION_FAILE:
        draft.getprice.error = action.error;
        break;
    }
  });

export default panelPagesReducer;
