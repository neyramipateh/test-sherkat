import styled from 'styled-components';
export const PanelStyles = styled.div`
  border: 1px solid #f1f1f1;
  height: 100%;
  margin: 10px;
  padding: inherit;
  .wraper-panel {
  }
`;

export const CreatePanelStyles = styled.div`
  margin: 10%;

  .wraper-panel {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(180px, 1fr));
    gap: 10%;
    grid-column-gap: 10%;
    grid-row-gap: 10%;
    row-gap: 10%;
    column-gap: 67px;
    padding: 10px;
    font-family: 'sia';
  }
  .txt-panle {
    gap: 10%;
    padding: 10px;
    border: 1px solid #8f007261;
    border-radius: 5px;
  }

  & .MuiInput-underline:after {
    left: 0;
    right: 0;
    bottom: 0;
    content: '';
    position: absolute;
    transition: transform 200ms cubic-bezier(0, 0, 0.2, 1) 0ms;
    border-bottom: 2px solid #3f51b5;
    pointer-events: none;
  }
`;

export const ShowPanelStyles = styled.div``;
