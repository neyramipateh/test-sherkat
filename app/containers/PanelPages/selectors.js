import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the panelPages state domain
 */

const selectPanelPagesDomain = state => state.panelPages || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by PanelPages
 */

const makeSelectPanelPages = () =>
  createSelector(
    selectPanelPagesDomain,
    substate => substate,
  );
export const makeGetpriceDataMultipale = () =>
  createSelector(
    selectPanelPagesDomain,
    substate => substate.getprice.data,
  );
export default makeSelectPanelPages;
export { selectPanelPagesDomain };
