/*
 *
 * PanelPages actions
 *
 */

import {
  GET_PRICE_ACTION,
  GET_PRICE_ACTION_SUCCESS,
  GET_PRICE_ACTION_FAILE,
} from './constants';

export function getPriceAction() {
  return {
    type: GET_PRICE_ACTION,
  };
}
export function getPriceSuccessAction(data) {
  return {
    type: GET_PRICE_ACTION_SUCCESS,
    data,
  };
}
export function getPriceFailAction(error) {
  return {
    type: GET_PRICE_ACTION_FAILE,
    error,
  };
}
