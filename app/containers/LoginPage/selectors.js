import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the loginPage state domain
 */

const selectLoginPageDomain = state => state.loginPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by LoginPage
 */

export const makeSelectLoginPage = () =>
  createSelector(
    selectLoginPageDomain,
    substate => substate.login,
  );

export const makeSelectUserRegister = () =>
  createSelector(
    selectLoginPageDomain,
    substate => substate.userregister,
  );

export const makeSelectVerifyCode = () =>
  createSelector(
    selectLoginPageDomain,
    substate => substate.verifycode,
  );

export const makeSelectActivationCode = () =>
  createSelector(
    selectLoginPageDomain,
    substate => substate.activationcode,
  );

export const makeSelectGoogleRegister = () =>
  createSelector(
    selectLoginPageDomain,
    substate => substate.googleregister,
  );

export { selectLoginPageDomain };
