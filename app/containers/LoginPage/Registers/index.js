/**
 *
 * Register
 *
 */
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  InputBase,
  Paper,
} from '@material-ui/core';
import { Lock, Person } from '@material-ui/icons';
import { isEmpty } from 'lodash';
import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import {
  activationVerifyCodeAction,
  sendUserRegisterAction,
  sendVerifyCodeAction,
} from '../actions';
import {
  makeSelectActivationCode,
  makeSelectUserRegister,
  makeSelectVerifyCode,
} from '../selectors';
import { RegisterWrap } from '../style';
export function Registers({
  handleToggleSubmit,
  sendregisterData,
  sendVerifyCode,
  activationCodes,
  toggleCode,
}) {
  const [error, setError] = useState(false);

  const [registerData, setRegisterData] = useState({
    code: null,
    password: null,
    email: null,
  });

  function handleSendMail() {
    if (
      !isEmpty(registerData.email, registerData.password) &&
      registerData.email.includes('@')
        ? 'email'
        : setError(!error)
    ) {
      toggleCode(activationCodes);
    }
    sendregisterData(registerData);
  }

  function changeData(key, value) {
    setRegisterData({ ...registerData, [key]: value });
  }

  function handleVerifyCode() {
    sendVerifyCode(registerData);
  }

  return (
    <RegisterWrap>
      <Grid xs={12}>
        <Card>
          <CardContent>
            <CardActions>
              {!activationCodes && (
                <Grid item xs={12}>
                  <Card>
                    <Grid className="actionArea">
                      <Grid item>
                        <span className="label">
                          اگر در آپارات حساب کاربری ندارید، ثبت نام کنید:
                        </span>
                      </Grid>

                      <Grid>
                        <Button
                          variant="contained"
                          color="secondary"
                          size="small"
                          fullWidth
                          onClick={handleToggleSubmit}
                        >
                          لاگین شوید
                        </Button>
                      </Grid>
                    </Grid>

                    <CardContent className="actionArea">
                      <div className="label">
                        اگر در آپارات حساب کاربری دارید، وارد شوید:
                      </div>

                      <Grid
                        spacing={2}
                        direction="row"
                        justify="center"
                        alignItems="flex-end"
                      >
                        <Grid item xs={12} sm={9}>
                          <Paper className="formInput">
                            <Person className="inputIcon" />
                            <InputBase
                              className="input"
                              placeholder="موبایل یا نام کاربری یا ایمیل"
                              defaultValue={registerData.email}
                              onChange={e =>
                                changeData('email', e.target.value.trim())
                              }
                            />
                          </Paper>

                          <Paper className="formInput">
                            <Lock className="inputIcon" />
                            <InputBase
                              className="input"
                              placeholder="گذرواژه خود را وارد کنید"
                              defaultValue={registerData.password}
                              onChange={e =>
                                changeData('password', e.target.value.trim())
                              }
                            />
                          </Paper>
                        </Grid>
                        <Grid item xs={12} sm={3}>
                          <Button
                            variant="contained"
                            color="secondary"
                            size="small"
                            fullWidth
                            onClick={handleSendMail}
                          >
                            ورود
                          </Button>
                        </Grid>
                      </Grid>
                    </CardContent>
                  </Card>
                </Grid>
              )}
            </CardActions>
            <CardActions>
              {activationCodes && (
                <Grid>
                  <CardActions>
                    <Paper className="formInput">
                      <Lock className="inputIcon" />
                      <InputBase
                        className="input"
                        placeholder="گذرواژه خود را وارد کنید"
                        defaultValue={registerData.code}
                        onChange={e =>
                          changeData('code', e.target.value.trim())
                        }
                      />
                    </Paper>
                  </CardActions>
                  <Grid item xs={12} sm={3}>
                    <Button
                      variant="contained"
                      color="secondary"
                      size="small"
                      fullWidth
                      onClick={handleVerifyCode}
                    >
                      ورود
                    </Button>
                  </Grid>
                </Grid>
              )}
            </CardActions>
          </CardContent>
        </Card>
      </Grid>
    </RegisterWrap>
  );
}

Registers.propTypes = {
  handleToggleSubmit: PropTypes.func.isRequired,
  sendregisterData: PropTypes.func.isRequired,
  sendVerifyCode: PropTypes.func.isRequired,
  toggleCode: PropTypes.func.isRequired,
  activationCodes: PropTypes.bool.isRequired,
};
const mapStateToProps = createStructuredSelector({
  userregister: makeSelectUserRegister(),
  codeVerify: makeSelectVerifyCode(),
  activationCodes: makeSelectActivationCode(),
});

const mapDispatchToProps = dispatch => ({
  sendregisterData: data => dispatch(sendUserRegisterAction(data)),
  sendVerifyCode: data => dispatch(sendVerifyCodeAction(data)),
  toggleCode: show => dispatch(activationVerifyCodeAction(show)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Registers);
