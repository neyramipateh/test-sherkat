/*
 *
 * LoginPage constants
 *
 */
export const ACTIVETION_VERIFY_CODE = 'app/LoginPage/ACTIVETION_VERIFY_CODE';
export const GOOGLE_GENERATE_USER = 'app/LoginPage/GOOGLE_GENERATE_USER';

export const LOGIN_ACTION = 'app/LoginPage/LOGIN_ACTION';
export const LOGIN_SUCCESS_ACTION = 'app/LoginPage/LOGIN_SUCCESS_ACTION';
export const LOGIN_FAIL_ACTION = 'app/LoginPage/LOGIN_FAIL_ACTION';
export const LOGIN_REINIT = 'app/LoginPage/LOGIN_REINIT';

export const SEND_USERREGISTER = 'app/LoginPage/SEND_USERREGISTER';
export const SEND_USERREGISTER_SUCCESS =
  'app/LoginPage/SEND_USERREGISTER_SUCCESS';
export const SEND_USERREGISTER_FAIL = 'app/LoginPage/SEND_USERREGISTER_FAIL';

export const GOOGLE_LOGIN_ALL = 'app/LoginPage/GOOGLE_LOGIN_ALL';
export const GOOGLE_LOGIN_SUCCESS = 'app/LoginPage/GOOGLE_LOGIN_SUCCESS';
export const GOOGLE_LOGIN_FAIL = 'app/LoginPage/GOOGLE_LOGIN_FAIL';

export const VERIFY_CODE = 'app/LoginPage/VERIFY_CODE';
export const VERIFY_CODE_SUCCESS = 'app/LoginPage/VERIFY_CODE_SUCCESS';
export const VERIFY_CODE_FAIL = 'app/LoginPage/VERIFY_CODE_FAIL';
