import { push } from 'connected-react-router';
import { errorHappenAction } from 'containers/App/actions';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { setAuth } from 'utils/auth';
import {
  googleRegisterApi,
  loginApi,
  registerUserApi,
  verifyCodeApi,
} from '../../api/auth';
import { getUserMe } from '../App/saga';
import { makeSelectUserMe } from '../App/selectors';
import {
  activationVerifyCodeAction,
  googleLoginFailActions,
  googleLoginSuccessActions,
  loginFailAction,
  loginSuccessAction,
  sendUserRegisterFaileAction,
  sendUserRegisterSuccessAction,
  sendVerifyCodeFaileAction,
  sendVerifyCodeSuccessAction,
} from './actions';
import {
  GOOGLE_LOGIN_ALL,
  LOGIN_ACTION,
  SEND_USERREGISTER,
  VERIFY_CODE,
} from './constants';

function* loginToServer({ username, password }) {
  try {
    const response = yield call(loginApi, { username, password });
    yield put(loginSuccessAction(response.data));
    console.log(response.data);
    setAuth(response.data);
    yield getUserMe();
    const userMe = yield select(makeSelectUserMe());
    console.log(userMe);
    if (userMe.error) {
      throw userMe.error;
    }
    setAuth({ ...response.data, me: userMe.data });
    yield put(push('/dashboard'));
  } catch (error) {
    if (error.response && error.response.status === 401) {
      yield put(loginFailAction(error.response));
    } else {
      yield put(errorHappenAction(error));
    }
  }
}

function* userRegister({ params }) {
  try {
    const response = yield call(registerUserApi, params);
    yield put(sendUserRegisterSuccessAction(response.data));
    yield put(activationVerifyCodeAction(true));
  } catch (error) {
    if (error.response && error.response.status === 422) {
      yield put(activationVerifyCodeAction(false));
      yield put(sendUserRegisterFaileAction(error.response));
    } else {
      yield put(errorHappenAction(error));
    }
  }
}

function* verifyCode({ params }) {
  try {
    const response = yield call(verifyCodeApi, params);
    yield put(sendVerifyCodeSuccessAction(response.data));
    setAuth(response.data);
    yield put(push('/dashboard'));
  } catch (error) {
    yield put(sendVerifyCodeFaileAction(error.response));
  }
}
function* googleLoginData({ params }) {
  try {
    const response = yield call(googleRegisterApi, params);
    yield put(googleLoginSuccessActions(response.data));
    yield setAuth({ ...response.data, me: params });
    yield put(push('/dashboard'));
  } catch (error) {
    yield put(googleLoginFailActions(error.response));
  }
}

export default function* loginPageSaga() {
  yield takeLatest(LOGIN_ACTION, loginToServer);
  yield takeLatest(SEND_USERREGISTER, userRegister);
  yield takeLatest(VERIFY_CODE, verifyCode);
  yield takeLatest(GOOGLE_LOGIN_ALL, googleLoginData);
}
