/**
 *
 * GoogleRegister
 *
 */

import { Grid } from '@material-ui/core';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';
import React, { memo, useCallback, useEffect, useState } from 'react';
import GoogleLogin from 'react-google-login';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { setAuth, getAuth } from 'utils/auth';

import { CLIENTID } from '../../../utils/constants';
import { googleLoginActions } from '../actions';
import { makeSelectGoogleRegister } from '../selectors';
import { GoogleStyle } from '../style';

function GoogleRegister({ sendToServer }) {
  const [googleData, setGoogleData] = useState(auths);
  const auths = getAuth();
  const responseGoogle = response => {
    sendGoogleData(response);
    console.log(response);
  };
  const responseFailGoogle = useCallback(
    response => {
      console.log(response);
    },
    [setGoogleData],
  );
  const sendGoogleData = response => {
    setGoogleData({
      ...googleData,
      name: response.profileObj.name,
      email: response.profileObj.email,
      googleId: response.profileObj.googleId,
      access_token: response.accessToken,
      expires_at: response.tokenObj.expires_at,
    });

    sendToServer({ ...googleData });
  };

  useEffect(() => {
    sendToServer();
  }, [setGoogleData]);
  return (
    <GoogleStyle>
      <Grid item xs={3}>
        <GoogleLogin
          clientId={CLIENTID}
          onSuccess={responseGoogle}
          onFailure={responseFailGoogle}
          buttonText="Login"
          cookiePolicy="single_host_origin"
          isSignedIn
          className="google-button"
          // onAutoLoadFinished={fetchData}
        />
      </Grid>
    </GoogleStyle>
  );
}

GoogleRegister.propTypes = {
  // googleregister: PropTypes.object.isRequired,
  sendToServer: PropTypes.func.isRequired,
  // redirect: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  googleregister: makeSelectGoogleRegister(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    redirect: path => dispatch(push(path)),
    sendToServer: data => dispatch(googleLoginActions(data)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
export default compose(
  withConnect,
  memo,
)(GoogleRegister);
