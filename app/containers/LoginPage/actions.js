/*
 *
 * LoginPage actions
 *
 */

import {
  ACTIVETION_VERIFY_CODE,
  GOOGLE_LOGIN_ALL,
  GOOGLE_LOGIN_FAIL,
  GOOGLE_LOGIN_SUCCESS,
  LOGIN_ACTION,
  LOGIN_FAIL_ACTION,
  LOGIN_REINIT,
  LOGIN_SUCCESS_ACTION,
  SEND_USERREGISTER,
  SEND_USERREGISTER_FAIL,
  SEND_USERREGISTER_SUCCESS,
  VERIFY_CODE,
  VERIFY_CODE_FAIL,
  VERIFY_CODE_SUCCESS,
} from './constants';

export function loginAction(username, password) {
  return {
    type: LOGIN_ACTION,
    username,
    password,
  };
}
export function loginSuccessAction(user) {
  return {
    type: LOGIN_SUCCESS_ACTION,
    user,
  };
}
export function loginFailAction(error) {
  return {
    type: LOGIN_FAIL_ACTION,
    error,
  };
}
export function loginReinitAction() {
  return {
    type: LOGIN_REINIT,
  };
}

export function sendUserRegisterAction(params) {
  return {
    type: SEND_USERREGISTER,
    params,
  };
}
export function sendUserRegisterSuccessAction(data) {
  return {
    type: SEND_USERREGISTER_SUCCESS,
    data,
  };
}
export function sendUserRegisterFaileAction(error) {
  return {
    type: SEND_USERREGISTER_FAIL,
    error,
  };
}

export function sendVerifyCodeAction(params) {
  return {
    type: VERIFY_CODE,
    params,
  };
}
export function sendVerifyCodeSuccessAction(data) {
  return {
    type: VERIFY_CODE_SUCCESS,
    data,
  };
}
export function sendVerifyCodeFaileAction(error) {
  return {
    type: VERIFY_CODE_FAIL,
    error,
  };
}
export function activationVerifyCodeAction(show) {
  return {
    type: ACTIVETION_VERIFY_CODE,
    show,
  };
}

export function googleLoginActions(params) {
  return {
    type: GOOGLE_LOGIN_ALL,
    params,
  };
}
export function googleLoginSuccessActions(data) {
  return {
    type: GOOGLE_LOGIN_SUCCESS,
    data,
  };
}
export function googleLoginFailActions(error) {
  return {
    type: GOOGLE_LOGIN_FAIL,
    error,
  };
}
