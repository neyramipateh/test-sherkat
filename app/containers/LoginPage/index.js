/**
 *
 * LoginPage
 *
 */

import {
  Button,
  Card,
  CardContent,
  Grid,
  InputBase,
  Paper,
} from '@material-ui/core';
import {
  ArrowForward as ArrowIcon,
  Person as PersonIcon,
  VpnKey as PasswordIcon,
} from '@material-ui/icons';
import Alert from 'components/Alert';
import { push } from 'connected-react-router';
import { isEmpty, isNull } from 'lodash';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import WebLayout from '../../layouts/Web';
import { getAuth } from '../../utils/auth';
import { HOME_ROUTE } from '../App/routes';
import { loginAction, loginReinitAction } from './actions';
import GoogleRegister from './GoogleRegister';
import reducer from './reducer';
import Registers from './Registers';
import saga from './saga';
import { makeSelectGoogleRegister, makeSelectLoginPage } from './selectors';
import { LoginWrapper } from './style';
// TODO cahnge to styled component
// TODO add AuthLayout

export function LoginPage({ onLoginSubmit, onHideError, data, redirect }) {
  useInjectReducer({ key: 'loginPage', reducer });
  useInjectSaga({ key: 'loginPage', saga });

  const [RegisterUsers, setRegisterUsers] = useState(false);
  // TODO مقادیر اولیه رو پاک کن
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  function handleToggleSubmit() {
    setRegisterUsers(!RegisterUsers);
    setUsername('');
    setPassword('');
  }
  const auth = getAuth();
  return (
    <WebLayout>
      <Grid xs={12}>
        {auth && !isNull(auth.mail) && !isEmpty(auth.mail) ? (
          redirect(HOME_ROUTE)
        ) : (
          <LoginWrapper>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Helmet>
                <title>login page</title>
                <meta name="description" content="Description of LoginPage" />
              </Helmet>

              <Grid item xs={12} className="wrapper">
                <Grid item xs={12}>
                  {/* <Logo className="logo" /> */}
                </Grid>

                <Grid>
                  <GoogleRegister />
                </Grid>

                <Grid item xs={12} className="headerIcons">
                  <Button
                    variant="outlined"
                    size="small"
                    className="backButton"
                  >
                    <ArrowIcon className="arrowIcon" />
                    بازگشت
                  </Button>

                  <Alert
                    show={!!data.error}
                    message="اطلاعات وارد شده مطابقت ندارد"
                    onClose={onHideError}
                  />
                </Grid>

                <Grid>
                  <div>
                    {RegisterUsers && (
                      <Registers handleToggleSubmit={handleToggleSubmit} />
                    )}
                  </div>

                  <div>
                    {!RegisterUsers && (
                      <div>
                        <Grid item xs={12}>
                          <Card>
                            <Grid container className="actionArea">
                              <Grid item xs={12} sm={8}>
                                <span className="label">
                                  اگر در سایت ثبت نام نکرده اید، ثبت نام کنید:
                                </span>
                              </Grid>

                              <Grid item xs={12} sm={4}>
                                <Button
                                  variant="contained"
                                  color="secondary"
                                  size="small"
                                  fullWidth
                                  onClick={handleToggleSubmit}
                                >
                                  ایجاد حساب کاربری
                                </Button>
                              </Grid>
                            </Grid>

                            <CardContent className="actionArea">
                              <div className="label">
                                اگر در آپارات حساب کاربری دارید، وارد شوید:
                              </div>

                              <Grid
                                container
                                spacing={2}
                                direction="row"
                                justify="center"
                                alignItems="flex-end"
                              >
                                <Grid item xs={12} sm={9}>
                                  <Paper className="formInput">
                                    <PersonIcon className="inputIcon" />
                                    <InputBase
                                      className="input"
                                      placeholder=" ایمیل خود را وارد کنید"
                                      defaultValue={username}
                                      onChange={e =>
                                        setUsername(e.target.value.trim())
                                      }
                                    />
                                  </Paper>

                                  <Paper className="formInput">
                                    <PasswordIcon className="inputIcon" />
                                    <InputBase
                                      className="input"
                                      placeholder="گذرواژه خود را وارد کنید"
                                      defaultValue={password}
                                      onChange={e =>
                                        setPassword(e.target.value.trim())
                                      }
                                    />
                                  </Paper>
                                </Grid>
                                <Grid item xs={12} sm={3}>
                                  <Button
                                    variant="contained"
                                    color="secondary"
                                    size="small"
                                    fullWidth
                                    onClick={() =>
                                      onLoginSubmit(username, password)
                                    }
                                  >
                                    ورود
                                  </Button>
                                </Grid>
                              </Grid>
                            </CardContent>
                          </Card>
                        </Grid>
                      </div>
                    )}
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </LoginWrapper>
        )}
        {/* <Dump data={data} /> */}
      </Grid>
    </WebLayout>
  );
}

LoginPage.propTypes = {
  data: PropTypes.object.isRequired,
  onLoginSubmit: PropTypes.func.isRequired,
  onHideError: PropTypes.func.isRequired,
  // googleSendToServer: PropTypes.func.isRequired,
  redirect: PropTypes.func.isRequired,
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  data: makeSelectLoginPage(),
  googleregister: makeSelectGoogleRegister(),
});

function mapDispatchToProps(dispatch) {
  return {
    onLoginSubmit: (username, password) =>
      dispatch(loginAction(username, password)),
    onHideError: () => dispatch(loginReinitAction()),
    redirect: path => dispatch(push(path)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(LoginPage);
