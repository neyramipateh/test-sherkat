/*
 *
 * LoginPage reducer
 *
 */
import produce from 'immer';
import {
  ACTIVETION_VERIFY_CODE,
  GOOGLE_LOGIN_ALL,
  GOOGLE_LOGIN_FAIL,
  GOOGLE_LOGIN_SUCCESS,
  LOGIN_ACTION,
  LOGIN_FAIL_ACTION,
  LOGIN_REINIT,
  LOGIN_SUCCESS_ACTION,
  SEND_USERREGISTER,
  SEND_USERREGISTER_FAIL,
  SEND_USERREGISTER_SUCCESS,
  VERIFY_CODE,
  VERIFY_CODE_FAIL,
  VERIFY_CODE_SUCCESS,
} from './constants';

export const initialState = {
  login: {
    username: null,
    password: null,
    user: null,
    error: null,
  },
  userregister: {
    params: null,
    data: null,
    error: null,
  },
  googl: {
    params: null,
    data: null,
    error: null,
  },
  verifycode: {
    params: null,
    data: null,
    error: null,
  },
  activationcode: false,

  googlegenerate: {
    data: null,
    error: null,
  },
};

/* eslint-disable default-case, no-param-reassign */
const loginPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOGIN_REINIT:
        draft.login.error = null;
        draft.login.user = null;
        break;
      case LOGIN_ACTION:
        draft.login.username = action.username;
        draft.login.password = action.password;
        draft.login.error = null;
        draft.login.user = null;
        break;
      case LOGIN_SUCCESS_ACTION:
        draft.login.user = action.user;
        draft.login.username = null;
        draft.login.password = null;
        draft.login.error = null;
        break;
      case LOGIN_FAIL_ACTION:
        draft.login.error = action.error;
        draft.login.user = null;
        draft.login.password = null;
        draft.login.username = null;
        break;

      case SEND_USERREGISTER:
        draft.userregister.params = action.params;
        draft.userregister.error = null;
        break;
      case SEND_USERREGISTER_SUCCESS:
        draft.userregister.data = action.data;
        draft.userregister.params = null;
        draft.userregister.error = null;
        break;
      case SEND_USERREGISTER_FAIL:
        draft.userregister.error = action.error;
        draft.userregister.data = null;
        draft.userregister.params = null;
        break;

      case ACTIVETION_VERIFY_CODE:
        draft.activationcode =
          action.show === undefined ? !draft.activationcode : action.show;
        break;

      case VERIFY_CODE:
        draft.verifycode.params = action.params;
        draft.verifycode.error = null;
        draft.verifycode.data = null;
        break;
      case VERIFY_CODE_SUCCESS:
        draft.verifycode.data = action.data;
        draft.verifycode.params = null;
        draft.verifycode.error = null;
        break;
      case VERIFY_CODE_FAIL:
        draft.verifycode.error = action.error;
        draft.verifycode.data = null;
        draft.verifycode.params = null;
        break;

      case GOOGLE_LOGIN_ALL:
        draft.googl.params = action.params;
        draft.googl.error = null;
        break;
      case GOOGLE_LOGIN_SUCCESS:
        draft.googl.data = action.data;
        draft.googl.params = null;
        draft.googl.error = null;
        break;
      case GOOGLE_LOGIN_FAIL:
        draft.googl.error = action.error;
        draft.googl.data = null;
        draft.googl.params = null;
        break;
    }
  });

export default loginPageReducer;
