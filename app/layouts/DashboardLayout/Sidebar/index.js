/**
 *
 * Sidebar
 *
 */

import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core';
import {
  AllInclusiveOutlined,
  Dashboard as DashboardIcon,
  ModeComment as CommentIcon,
  MovieOutlined,
  PowerSettingsNew as LoguotIcon,
  Settings,
  SupervisorAccountOutlined,
} from '@material-ui/icons';
import MarkunreadMailboxIcon from '@material-ui/icons/MarkunreadMailbox';
import { push } from 'connected-react-router';
import { logoutAction } from 'containers/App/actions';
import {
  DASHBOARD_ROUTE,
  LOGIN_ROUTE,
  MULTIPLE,
  PRODUCT_MANEGER,
  ROUTE_COMMENTS,
  ROUTE_MANAGE_PANEL,
  ROUTE_MANAGE_SMS,
  ROUTE_MANAGE_USERS,
  ROUTE_SUBLINE,
  SITE_MANEGER,
} from 'containers/App/routes';
import {
  makeSelectLocation,
  makeSelectLogout,
  makeSelectUserMe,
} from 'containers/App/selectors';
import PropTypes from 'prop-types';
import React from 'react';
import { connect, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';
import { getAuth, isAdminUser } from '../../../utils/auth';
import '../../dana.css';

const Wrapper = styled.div`
  width: 180px;
  font-family: 'majid';
  font-style: revert;
  font-weight: 300;
  background: #fff;
  box-shadow: -1px 2px 2px #eee;
  min-height: calc(100vh - 50px);

  & .channelSetting {
    background: #fff;
    display: block;
    text-align: center;
    margin-bottom: 10vh;
    cursor: pointer;

    .userAvatar {
      width: 140px;
      height: 140px;
      padding: 0.5em;
      border: 3px solid #eee;
      border-radius: 100%;
    }
  }

  & .channelSetting .MuiSvgIcon-root,
  & .channelSetting .MuiListItemText-root {
    display: block;
    width: 100%;
    margin: auto;
    text-align: center;
  }

  & .channelSetting .MuiSvgIcon-root {
    font-size: 120px;
    width: 120px;
    color: #e5e5e5;
  }

  & .channelSetting svg {
    background: #fff;
    box-shadow: 0 0 2px 1px #e2dfdf;
    border-radius: 100%;
    padding: 0;
    display: block;
  }

  & .channelSetting .MuiTypography-root {
    font-weight: bold;
  }

  & .MuiSvgIcon-root,
  & .MuiListItemText-root {
    color: #1d101e;
  }

  & .MuiListItemText-root {
    font-family: 'majid';
    font-style: revert;
    font-weight: 300;
    font-size: 1rem;
  }

  & .MuiListItemIcon-root {
    min-width: 30px;
  }

  & .MuiListItemText-root {
    text-align: right;
  }

  & .settingIcon {
    font-family: 'majid';
    font-style: revert;
    font-weight: 300;
    position: absolute;
    left: 32px;
    top: 22px;
    background: #e5e5e5 !important;
    border: 1px solid #e1e1e1 !important;
    cursor: pointer;
    transition: opacity 130ms ease;
  }

  & .settingIcon,
  & .settingIcon svg {
    font-size: 20px !important;
    width: 20px !important;
    color: #6f7285 !important;
  }

  & .settingIcon:hover {
    opacity: 0.8;
  }

  & .logoutItem {
    position: relative;
    bottom: 0;
  }

  @media (max-width: 768px) {
    & {
      display: none;
    }
  }

  @media (max-height: 560px) {
    & .channelSetting {
      margin-bottom: 0vh;
    }
  }
`;

function Sidebar({ logoutData, location, redirect }) {
  const dispatch = useDispatch();
  const isAdmin = isAdminUser();
  const Auth = getAuth();

  return (
    <Wrapper>
      <List component="nav">
        <ListItem
          button
          selected={DASHBOARD_ROUTE === location.pathname}
          onClick={() => dispatch(push(DASHBOARD_ROUTE))}
        >
          <ListItemIcon>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary="داشبرد" />
        </ListItem>

        <div>
          {isAdmin && <Divider />}
          {
            <ListItem
              button
              selected={ROUTE_MANAGE_USERS === location.pathname}
              onClick={() => dispatch(push(ROUTE_MANAGE_USERS))}
            >
              <ListItemIcon>
                <SupervisorAccountOutlined />
              </ListItemIcon>
              <ListItemText primary="مدیریت کاربران" />
            </ListItem>
          }
        </div>
        <div>
          {
            <ListItem
              button
              selected={ROUTE_MANAGE_PANEL === location.pathname}
              onClick={() => dispatch(push(ROUTE_MANAGE_PANEL))}
            >
              <ListItemIcon>
                <MarkunreadMailboxIcon />
              </ListItemIcon>
              <ListItemText primary="مدیریت بسته ها" />
            </ListItem>
          }
        </div>
        <div>
          {
            <ListItem
              button
              selected={ROUTE_MANAGE_PANEL === location.pathname}
              onClick={() => dispatch(push(ROUTE_SUBLINE))}
            >
              <ListItemIcon>
                <AllInclusiveOutlined />
              </ListItemIcon>
              <ListItemText primary="مدیریت خطوط" />
            </ListItem>
          }
        </div>
        <div>
          <ListItem
            button
            selected={MULTIPLE === location.pathname}
            onClick={() => dispatch(push(MULTIPLE))}
          >
            <ListItemIcon>
              <SupervisorAccountOutlined />
            </ListItemIcon>
            <ListItemText primary="تعرفه" />
          </ListItem>
        </div>
        <div>
          <ListItem
            button
            selected={ROUTE_MANAGE_SMS === location.pathname}
            onClick={() => dispatch(push(ROUTE_MANAGE_SMS))}
          >
            <ListItemIcon>
              <SupervisorAccountOutlined />
            </ListItemIcon>
            <ListItemText primary="مدیریت پیام" />
          </ListItem>
        </div>
        <ListItem
          button
          selected={PRODUCT_MANEGER === location.pathname}
          onClick={() => dispatch(push(PRODUCT_MANEGER))}
        >
          <ListItemIcon>
            <MovieOutlined />
          </ListItemIcon>
          <ListItemText primary="مدیریت محصولات" />
        </ListItem>
        <ListItem
          button
          selected={ROUTE_COMMENTS === location.pathname}
          onClick={() => dispatch(push(ROUTE_COMMENTS))}
        >
          <ListItemIcon>
            <CommentIcon />
          </ListItemIcon>
          <ListItemText primary="دیدگاه ها" />
        </ListItem>
        <ListItem
          button
          className="settings"
          disabled={logoutData.loading}
          onClick={() => redirect(SITE_MANEGER)}
        >
          <ListItemIcon>
            <Settings />
          </ListItemIcon>
          <ListItemText primary="تنظیمات سایت" />
        </ListItem>
      </List>

      <ListItem
        button
        className="logoutItem"
        disabled={logoutData.loading}
        onClick={() => dispatch(logoutAction())}
      >
        <ListItemIcon>
          <LoguotIcon />
        </ListItemIcon>
        <ListItemText primary="خروج ازحساب کاربری" />
      </ListItem>
    </Wrapper>
  );
}

Sidebar.propTypes = {
  location: PropTypes.any.isRequired,
  logoutData: PropTypes.object.isRequired,
  redirect: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  location: makeSelectLocation(),
  user: makeSelectUserMe(),
  logoutData: makeSelectLogout(),
});
function mapDispatchToProps(dispatch) {
  return {
    redirect: path => dispatch(push(path)),
  };
}

const withRouter = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default withRouter(Sidebar);
