/**
 *
 * DashboardLayout
 *
 */

import { Grid } from '@material-ui/core';
// import { getCategoriesAction, getTagsAction } from 'containers/App/actions';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import styled from 'styled-components';
import NavBar from './NavBar';
import Sidebar from './Sidebar';
import SidebarDrawer from './SidebarDrawer';
import '../dana.css';
const StyledDashboardWrapper = styled.div`
  font-family: 'yara';
  background: #fefefe;
  color: #6f7285;

  & .sidebarWrapper {
    font-family: yara;
    z-index: 0;
    padding-bottom: 0px !important;
  }

  & .contentWrapper {
    font-family: yara;
    padding: 15px;
    flex: 1;
  }
  & .MuiTypography-body1 {
    font-family: sia;
    font-size: 16px;
    font-weight: bold;
    border-bottom: 1px solid #e5d8cfa8;
    text-align: -webkit-center;
    width: 100%;
  }
`;

function DashboardLayout({ children, showSidebar }) {
  // useEffect(() => {
  //   getTagsFromServer();
  //   getCategoriesFromServer();
  // }, []);

  return (
    <StyledDashboardWrapper>
      <NavBar />
      <SidebarDrawer />

      <Grid container>
        {showSidebar && (
          <Grid className="sidebarWrapper">
            <Sidebar />
          </Grid>
        )}
        <Grid className="contentWrapper">{children}</Grid>
      </Grid>
    </StyledDashboardWrapper>
  );
}

DashboardLayout.propTypes = {
  children: PropTypes.node.isRequired,
  showSidebar: PropTypes.bool,
  //   getTagsFromServer: PropTypes.func.isRequired,
  //   getCategoriesFromServer: PropTypes.func.isRequired,
};

DashboardLayout.defaultProps = {
  showSidebar: true,
};

function mapDispatchToProps() {
  return {
    // getTagsFromServer: () => dispatch(getTagsAction()),
    // getCategoriesFromServer: () => dispatch(getCategoriesAction()),
  };
}
const withStore = connect(
  undefined,
  mapDispatchToProps,
);
export default compose(
  memo,
  withStore,
)(DashboardLayout);
