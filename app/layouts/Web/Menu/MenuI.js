import { MenuItem } from '@material-ui/core';
import { push } from 'connected-react-router';
import React, { memo } from 'react';
import { useDispatch } from 'react-redux';
import { LOGIN_ROUTE } from '../../../containers/App/routes';
import { MenuiStyles } from '../styles';

function Menui() {
  const dispatch = useDispatch();
  return (
    <MenuiStyles>
      <MenuItem label="about" className="listein" value="favorites">
        About
      </MenuItem>

      <MenuItem label="Products" className="listein" value="favorites">
        Products
      </MenuItem>

      <MenuItem label="blogs" className="listein" value="favorites">
        blogs
      </MenuItem>

      <MenuItem
        label="LOgin"
        className="listein"
        value="favorites"
        onClick={() => dispatch(push(LOGIN_ROUTE))}
      >
        LOgin
      </MenuItem>
    </MenuiStyles>
  );
}

export default memo(Menui);
