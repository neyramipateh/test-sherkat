import { Menu, MenuItem } from '@material-ui/core';
import { HomeSharp, ViewList } from '@material-ui/icons';
import React, { memo, useState } from 'react';
import { HeaderStyles, MenunStyles } from '../styles';

function Menun() {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <MenunStyles>
      <ViewList
        className=""
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      />
      <Menu
        className=""
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem label="about" className="home-menu" value="favorites">
          About
        </MenuItem>
        <MenuItem label="about" className="home-menu" value="favorites">
          Products
        </MenuItem>
        <MenuItem label="about" className="home-menu" value="favorites">
          blogs
        </MenuItem>
      </Menu>
    </MenunStyles>
  );
}

export default memo(Menun);
