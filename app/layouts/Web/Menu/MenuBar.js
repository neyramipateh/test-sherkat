import { MenuItem } from '@material-ui/core';
import { HomeSharp } from '@material-ui/icons';
import { push } from 'connected-react-router';
import React from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { BASEURL } from '../../../constanse/constance';
import { HOME_ROUTE } from '../../../containers/App/routes';
import Menun from './menun';
import Menui from './MenuI';
const MenuBarStyles = styled('section')`
  width: 100%;
  position: relative;
  clear: both;
  margin: 1em;
  display: grid;
  justify-content: right;
  grid-template-columns: 1fr 1fr 5fr;

  & .magic-link {
    justify-content: center;
    margin: auto;
  }

  @media only screen and (max-width: 768px) {
    .links {
      display: none;
    }
  }
`;
function MenuBar() {
  const dispatch = useDispatch();
  return (
    <MenuBarStyles>
      <div className="magic-home">
        <MenuItem
          label="home"
          variant="contained"
          className="home-menu"
          value={BASEURL}
          onClick={() => dispatch(push(HOME_ROUTE))}
        >
          <HomeSharp />
        </MenuItem>
      </div>

      <div className="magic-link">
        <Menun />
      </div>
      <div className="links">
        <Menui />
      </div>
    </MenuBarStyles>
  );
}

MenuBar.propTypes = {};

export default MenuBar;
