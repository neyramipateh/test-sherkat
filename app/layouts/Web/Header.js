/* eslint-disable no-console */
//* *هدر سایت */

import { Grid } from '@material-ui/core';
import { PhoneIphoneSharp } from '@material-ui/icons';
import { mdiGmail as gmail } from '@mdi/js';
import Icon from '@mdi/react';
import React, { useEffect, useState } from 'react';
import { getSetting } from '../../containers/DashboardPage/StorageSetting/SettingData';
import SearchBar from '../DashboardLayout/SearchBar';
import { HeaderStyles } from './styles';

const Header = () => {
  const localData = getSetting();
  const [khadamat, setKhadamat] = useState(null);

  useEffect(function persistForm() {
    // 👍 We're not breaking the first rule anymore
    if (localData !== '') {
      setKhadamat(localData);
    }
  }, []);

  return (
    <HeaderStyles>
      <Grid container>
        <div className="header">
          <div className="toolbar">
            <div className="section">
              <div className="col grid_1_of_2">
                {khadamat && (
                  <div className="up-divace">
                    <div className="phon">
                      <span className="phon-Icons">{khadamat.phone}</span>
                      <PhoneIphoneSharp />
                    </div>
                    <div>
                      <span className="mail-text">{khadamat.mail}</span>
                      <Icon path={gmail} size={1} className="right-color" />
                    </div>
                  </div>
                )}
              </div>
              <div className="col grid_1_of_2">
                <div className="serach-header">
                  <SearchBar
                    onRequestSearch={() => console.log('onRequestSearch')}
                    style={{
                      margin: '0 auto',
                      maxWidth: 800,
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Grid>
    </HeaderStyles>
  );
};

Header.propTypes = {};

export default Header;
