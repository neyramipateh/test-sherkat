import React from 'react';
import { Grid } from '@material-ui/core';
import styled from 'styled-components';

import MenuBar from '../Menu/MenuBar';

const StyledNav = styled.div`
  /* position: relative; */
  .nav-img {
    background-color: #e4e4da;
    height: 263px;
    width: 100%;
  }
  .row {
    position: absolute;
    top: 100%;
  }
  .logo {
    width: 100%;
  }
  .logo-header {
    align-items: center;
    margin: auto;
  }
  .logo-in {
    position: absolute;
    width: 143px;
    top: 22%;
    left: 45%;
  }
`;
const Navbar = () => (
  <StyledNav>
    <Grid container>
      <div>{/* <img src={image} alt="header" className="nav-img" /> */}</div>
      <Grid item xs={12}>
        <div className="container">
          <MenuBar />
        </div>
      </Grid>
      <Grid item xs={12}>
        <div className="col-4 logo-header">
          {/* <img src={logo} alt="header" className="logo" /> */}
        </div>
      </Grid>
    </Grid>
  </StyledNav>
);

export default Navbar;
