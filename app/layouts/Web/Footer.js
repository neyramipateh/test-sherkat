import { Grid, MenuItem, MenuList } from '@material-ui/core';
import { Facebook, LinkedIn, PhoneAndroid, Twitter } from '@material-ui/icons';
import { mdiGooglePlus } from '@mdi/js';
import Icon from '@mdi/react';
import React, { memo } from 'react';

import { FooterStyles } from './styles';

function Footer() {
  return (
    <FooterStyles>
      <Grid container>
        <div className=" divace">
          <Grid item xs={12}>
            <div className="row all-left">
              <div className="col">
                <MenuList>
                  <MenuItem>
                    <Facebook variant="outlined" />
                  </MenuItem>
                </MenuList>
              </div>

              <div className="col">
                <MenuList>
                  <MenuItem>
                    <Icon
                      path={mdiGooglePlus}
                      size={1}
                      className="right-color"
                    />
                  </MenuItem>
                </MenuList>
              </div>

              <div className="col">
                <MenuList>
                  <MenuItem>
                    <LinkedIn variant="outlined" />
                  </MenuItem>
                </MenuList>
              </div>

              <div className="col">
                <MenuList>
                  <MenuItem>
                    <Twitter variant="outlined" />
                  </MenuItem>
                </MenuList>
              </div>
            </div>
          </Grid>
          <Grid item xs={12}>
            {/* <img src={image} alt="header" className={classes.center} /> */}
          </Grid>
          <Grid item xs={12}>
            <div className="hoghoghi-footer">
              <span>کلیه حقوق این سایت متعلق به شرکت سفیر تریلر میباشد</span>
            </div>
            {/* <Linex title="سوشیال" /> */}
            <div className="desgin-footer">
              <span className="desginer">
                <p>:Desginer </p> Neyrami.65@gmail.com
              </span>

              <p>
                09354274451
                <PhoneAndroid />
              </p>
            </div>
          </Grid>
        </div>
      </Grid>
    </FooterStyles>
  );
}

export default memo(Footer);
