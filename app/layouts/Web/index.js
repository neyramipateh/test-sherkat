/*
 *
 * WebLayout
 *
 */
import { Container, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import Footer from './Footer';
import Header from './Header';
import Navbar from './nav';
import '../dana.css';
const WebStyles = styled.div`
  font-family: neyrami;
  justify-content: center;
  margin: auto;
  & .contentWrapper {
    padding: 15px;
    flex: 1;
  }
`;
function WebLayout({ children, title }) {
  return (
    <WebStyles>
      <Helmet>{title}</Helmet>
      <Container>
        <Grid item xs={12}>
          <Header />
        </Grid>
        <Grid item xs={12}>
          <Navbar />
        </Grid>
        <div className="contentWrapper">{children}</div>
        <Footer />
      </Container>
    </WebStyles>
  );
}

WebLayout.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
};

export default memo(WebLayout);
