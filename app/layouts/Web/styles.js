import styled from 'styled-components';

export const HeaderStyles = styled.div`
  background-color: #fff;

  .header {
    width: 100%;
    position: relative;
    height: auto;
    grid-column: col-start / span 12;
  }
  .toolbar {
    position: absolute;
    width: 100%;
    transform: translateY(-5%);
    background-color: #33333391;
    top: 5%;
  }
  .up-divace {
    grid-template-columns: repeat(auto-fit, minmax(210px, 2fr));
    grid-template-rows: repeat(auto-fit, minmax(2em, 2fr));
    display: grid;
    gap: 10px;
    grid-auto-columns: auto;
    align-items: flex-end;
    justify-items: center;
    color: #ccc967;
    grid-column: 2;
    margin: 1em;
    border-bottom: 1px solid #b3d4fc;
    border-radius: 25px;
  }
  .header_left_col {
    margin: 1rem;
  }
  .all-header {
  }
  .serach-header {
    margin: 1em;
    transform: translateY(0%);
    top: 50%;
    width: 90%;
    padding-right: 9px;
  }
  .phon {
    border-radius: 9px;
    padding-left: 9px;
  }
`;

export const FooterStyles = styled.div`
  background-color: #fdfdf4;
  position: relative;
  height: 151px;
  color: darkcyan;
  & .MuiGrid-root {
    /* border: 1px solid #556; */
  }
  .section {
    clear: both;
    margin: 0px;
    padding: 0px;
  }
  /* Column */
  .col {
    background: #fff;
    display: block;
    float: left;
    margin: 1% 0 1% 1.6%;
  }
  .col:first-child {
    margin-left: 0 !important;
  }

  /* Row */
  .row:before,
  .row:after {
    content: '';
    display: table;
  }
  .row:after {
    clear: both;
  }
  .row {
    zoom: 1; /* For IE 6/7 */
  }

  /* Full Width below 768 pixels */
  @media only screen and (max-width: 768px) {
    .col {
      margin: 1% 0 1% 0%;
    }
    [class*='grid_'] {
      width: 100%;
    }
  }

  .divace {
    grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
    display: grid;
    position: relative;
  }
  .all-left {
    justify-content: space-between;
    display: flex;
    border: 1px solid #efe;
    transform: translateX(0);
    padding: 2px;
    width: 100%;
  }

  .hoghoghi-footer {
    bottom: 0;
    margin: 1rem;
  }
  .hoghoghi-footer span {
    bottom: 0;
    top: 50%;
    font-size: large;
  }
  .desgin-footer {
    text-align: center;
    clear: both;
    margin: 1rem;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(150px, 3fr));
  }
  .desgin-footer span {
    margin: auto;
    color: #66cc44;
    font-family: cursive;
  }
  .desgin-footer p {
    color: #66cc44;
    font-family: cursive;
  }
`;

export const MenuiStyles = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-evenly;
  .liston {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
  }
  .listein {
    border: 1px solid #c94;
    border-radius: 52%;
  }
`;
export const MenunStyles = styled.div`
  color: cadetblue;
  padding: 6px 16px;
`;
