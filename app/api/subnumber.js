import request from 'utils/request';

export function getSubnumberApi() {
  const config = {
    method: 'get',
    url: '/admin/subnumber',
  };

  return request(config);
}
export function getPriceApi(data) {
  const config = {
    method: 'get',
    url: '/admin/price',
    data,
  };

  return request(config);
}
export function getPriceDataApi() {
  const config = {
    method: 'get',
    url: '/admin/price/data',
  };

  return request(config);
}
