import request from 'utils/request';

export const sendMessageApi = ({ MsgBody, Operator, To, types, lengths }) => {
  const config = {
    method: 'post',
    url: '/admin/message/create',
    data: { MsgBody, Operator, To, types, lengths },
  };
  return request(config);
};
export const getSendingMessageApi = params => {
  const config = {
    method: 'get',
    url: `/admin/message?${params.page || 1}&per_page=${params.size || 10}`,
  };
  return request(config);
};

export const getSendingStatusMessageApi = ({ slug, state }) => {
  const config = {
    method: 'post',
    url: `/admin/message/active/${slug}`,
    data: { state },
  };
  return request(config);
};
