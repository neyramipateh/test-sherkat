import request from 'utils/request';

export function getUsersApi(params) {
  const config = {
    method: 'get',
    url: `/admin/user?page=${params.page || 1}&per_page=${params.size || 10}`,
  };

  return request(config);
}

export function getUserMeApi() {
  const config = {
    method: 'get',
    url: '/admin/user/me',
  };

  return request(config);
}

export function updateUserApi(params) {
  const { id, ...data } = params;

  const config = {
    method: 'put',
    url: `/admin/user/${id}`,
    data,
  };
  return request(config);
}

export function resetUserPasswordApi(params) {
  const { id } = params;

  const config = {
    method: 'put',
    url: `/admin/user/${id}/reset-password`,
  };

  return request(config);
}

export function unregisterUserApi() {
  const config = {
    method: 'delete',
    url: '/admin/user/me',
  };

  return request(config);
}

export function deleteUserApi(params) {
  const config = {
    method: 'delete',
    url: `/admin/user/${params.id}`,
  };

  return request(config);
}

export function logoutApi() {
  const config = {
    method: 'get',
    url: '/auth/logout',
  };

  return request(config);
}
