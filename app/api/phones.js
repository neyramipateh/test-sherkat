import request from 'utils/request';

export const getPhonesApi = () => {
  const config = {
    method: 'get',
    url: '/admin/phone',
  };
  return request(config);
};
