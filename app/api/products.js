import request from 'utils/request';

export function getProductApi(params) {
  const config = {
    method: 'get',
    url: `/admin/product?${params.page || 1}&per_page=${params.size || 10}`,
  };

  return request(config);
}

export function showProductApi() {
  const config = {
    method: 'get',
    url: '/product/show',
  };

  return request(config);
}

export function updateProductApi(params) {
  const { id, ...data } = params;
  const config = {
    method: 'put',
    url: `/admin/product/update/${id}`,
    data,
  };

  return request(config);
}

export function createProductApi(data) {
  const config = {
    method: 'post',
    url: '/admin/product/create',
    data,
  };

  return request(config);
}
export function sendProductIdApi(slug) {
  const config = {
    method: 'get',
    url: `viewid/${slug}`,
  };

  return request(config);
}

export function uploadBannerApi(file) {
  const data = new FormData();
  data.append('banner', file);

  const config = {
    method: 'post',
    url: '/uploadbanner',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data,
  };

  return request(config);
}

export function sliderShowApi() {
  const config = {
    method: 'get',
    url: '/slider',
  };

  return request(config);
}
