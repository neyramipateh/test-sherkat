/* eslint-disable camelcase */
import request from 'utils/request';

export function setTarifApi({ name, price, subnumber }) {
  const config = {
    method: 'post',
    url: '/admin/price/create',
    data: { name, price, subnumber },
  };
  return request(config);
}
