/* eslint-disable camelcase */
import request from 'utils/request';

export function loginApi({ username, password }) {
  const config = {
    method: 'post',
    url: '/auth/login',
    data: {
      username,
      password,
    },
  };

  return request(config);
}

export function registerUserApi({ email, password }) {
  const config = {
    method: 'post',
    url: '/auth/register',
    data: { email, password },
  };

  return request(config);
}

export function verifyCodeApi({ code }) {
  const config = {
    method: 'post',
    url: '/auth/verify',
    data: { code },
  };

  return request(config);
}

export function googleRegisterApi({
  name,
  email,
  password,
  access_token,
  expires_at,
  googleId,
}) {
  const config = {
    method: 'post',
    url: '/auth/google/register',
    data: { name, email, password, access_token, expires_at, googleId },
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };

  return request(config);
}
