import request from 'utils/request';

export function getCategoriesApi() {
  const config = {
    method: 'get',
    url: '/category',
  };

  return request(config);
}

export function addCategoriesApi(category) {
  const config = {
    method: 'post',
    url: '/category/create',
    data: { title: category },
  };

  return request(config);
}
