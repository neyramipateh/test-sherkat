import request from 'utils/request';

export function getSubNameApi() {
  const config = {
    method: 'get',
    url: '/admin/subname',
  };

  return request(config);
}
export function getCategortApi() {
  const config = {
    method: 'get',
    url: '/admin/category/phone',
  };

  return request(config);
}
export function getRangeApi() {
  const config = {
    method: 'get',
    url: '/admin/range',
  };

  return request(config);
}
export function sendSubLineApi({
  category,
  price,
  range,
  subnames,
  subnumbers,
}) {
  const config = {
    method: 'post',
    url: '/admin/subline/create',
    data: { category, price, range, subnames, subnumbers },
  };

  return request(config);
}
export function sendSubCategoryApi({ id, persian, latin, irancell, mci }) {
  const config = {
    method: 'post',
    url: '/admin/subcategory/create',
    data: { id, persian, latin, irancell, mci },
  };

  return request(config);
}
export function addSubNameSubLinersApi({ title }) {
  const config = {
    method: 'post',
    url: '/admin/subname/create',
    data: { title },
  };

  return request(config);
}

export const getSublinersPageApi = params => {
  const config = {
    method: 'get',
    url: `/admin/subline/show?${params.page || 1}&per_page=${params.size ||
      10}`,
  };
  return request(config);
};
