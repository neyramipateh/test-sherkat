// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getStorage } from 'firebase/storage';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// const firebaseConfig = {
//   apiKey: 'AIzaSyB1QS8dYNHb_HyIZEmWpxSE0BpcD3TJsA4',
//   authDomain: 'operate-c934d.firebaseapp.com',
//   projectId: 'operate-c934d',
//   storageBucket: 'operate-c934d.appspot.com',
//   messagingSenderId: '782220968942',
//   appId: '1:782220968942:web:70f58c1583e483131044ff',
// };

const firebaseConfig = {
  apiKey: 'AIzaSyDbIkwhjkEquxawrGuZzfGxG5EsAf5tDB8',
  authDomain: 'operate-manage-image.firebaseapp.com',
  projectId: 'operate-manage-image',
  storageBucket: 'operate-manage-image.appspot.com',
  messagingSenderId: '856579647279',
  appId: '1:856579647279:web:cd4795abbed793257b23da',
};
// Initialize Firebase
export const app = initializeApp(firebaseConfig);

export const storage = getStorage();
